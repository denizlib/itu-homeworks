/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 18.09.2014
*/
#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <ctype.h>
#include "fileop.h"

using namespace std;

Dosya defter;

void menu_yazdir();
bool islem_yap(char);
void kayit_ad_ara();
void kayit_dept_ara();
void kayit_ekle();
void kayit_sil();
void kayit_guncelle();
void bastir();

int main(){
    defter.olustur();
    bool bitir = false;
    char secim;
    while (!bitir) {
        menu_yazdir();
        cin >> secim;
        bitir = islem_yap(secim);
    }
    defter.kapat();
    return EXIT_SUCCESS;
}

void menu_yazdir(){
    //system("clear");
    cout << endl << endl;
    cout << "Faculty Records Application" << endl;
    cout << "Choose an option" << endl;
    cout << "A: Add Record" << endl;
    cout << "S: Search Records by Department" << endl;
    cout << "F: Search Records by First Name" << endl;
    cout << "U: Upgrade Record" << endl;
    cout << "R: Delete Record" << endl;
    cout << "P: Print all the Records" << endl;
    cout << "E: Exit" << endl;
    cout << endl;
    cout << "Enter your option {A,S,F,U,R,P,E} : ";
}

bool islem_yap(char secim){
    bool sonlandir=false;
    switch (secim) {
        case 'F': case 'f':
            kayit_ad_ara();
            break;
        case 'S': case 's':
            kayit_dept_ara();
            break;
        case 'A': case 'a':
            kayit_ekle();
            break;
        case 'U': case 'u':
            kayit_guncelle();
            break;
        case 'R': case 'r':
            kayit_sil();
            break;
        case 'P': case 'p':
            bastir();
            break;
        case 'E': case 'e':
            cout << "Are you sure that you want to terminate the program? (Y/N):";
            cin >> secim;
            if(secim=='Y' || secim=='y')
                sonlandir=true;
                break;
        default:
            cout << "Error: You have made an invalid choice" << endl;
            cout << "Try again {A, E, G, S, C} :" ;
            cin >> secim;
            sonlandir = islem_yap(secim);
            break;
    }
    return sonlandir;
}

void kayit_ad_ara(){
    char fname[15];
    cout << "Please enter the name of the person you want to search (press '*' for listing all):" << endl;
    cin.ignore(1000, '\n');
    cin.getline(fname,15);
    if(defter.araAd(fname)==0){
        cout << "Record can not be found" << endl;
    }
    getchar();
};
void kayit_dept_ara(){
    char dept[5];
    cout << "Please enter the name of the person you want to search (press '*' for listing all):" << endl;
    cin.ignore(1000, '\n');
    cin.getline(dept,5);
    if(defter.araDept(dept)==0){
        cout << "Record can not be found" << endl;
    }
    getchar();
};

void kayit_ekle(){
    Faculty_Record yenikayit;
    cout << "Please enter the information of the person you want to save " << endl;
    cout << "First Name : " ;
    cin.ignore(1000, '\n');
    cin.getline(yenikayit.fname,15);
    cout << "Last name :";
    cin >> yenikayit.lname;
    cout << "Phone number :";
    cin >> yenikayit.phone;
    cout << "Office number :";
    cin >> yenikayit.office;
    cout << "Department :";
    cin >> yenikayit.dept;
    defter.ekle(&yenikayit);
    cout << "Record has been added" << endl;
    getchar();
};

void kayit_guncelle(){
    char fname[15];
    int secim;
    char tercih;
    cout << "Please enter the name of the person you want to update (press '*' for listing all):" << endl;
    cin.ignore(1000, '\n');
    cin.getline(fname,15);
    int kisisayisi=defter.araAd(fname);
    if(kisisayisi==0){
        cout << "Record can not be found" << endl;
    }
    else {
        if (kisisayisi==1){
            cout << "Record has been found." << endl;
            cout << "Please enter the index of the record if you want to update this contact (Press -1 to exit without updating) " ;
        }
        else
            cout << "Please enter the index of the record that you want to update (Press -1 to exit without updating): " ;
        cin >> secim;
        if(secim==-1) return;
        Faculty_Record kayit=defter.getir(secim);
        cout << "Do you want to update the first name? (Y/N)" << endl;
        cin >> tercih;
        switch (tercih) {
            case 'Y': case 'y':
                cout << "New First Name : " ;
                cin.ignore(1000, '\n');
                cin.getline(kayit.fname,15);
                break;
            case 'N': case 'n':
                break;
        }
        cout << "Do you want to update the last name? (Y/N)" << endl;
        cin >> tercih;
        switch (tercih) {
            case 'Y': case 'y':
                cout << "New Last Name : " ;
                cin.ignore(1000, '\n');
                cin.getline(kayit.lname,15);
                break;
            case 'N': case 'n':
                break;
        }
        cout << "Do you want to update the phone number? (Y/N)" << endl;
        cin >> tercih;
        switch (tercih) {
            case 'Y': case 'y':
                cout << "New Phone Number : " ;
                cin.ignore(1000, '\n');
                cin>>kayit.phone;
                break;
            case 'N': case 'n':
                break;
        }
        cout << "Do you want to update the office number? (Y/N)" << endl;
        cin >> tercih;
        switch (tercih) {
            case 'Y': case 'y':
                cout << "New Office Number : " ;
                cin.ignore(1000, '\n');
                cin>>kayit.office;
                break;
            case 'N': case 'n':
                break;
        }
        cout << "Do you want to update the department? (Y/N)" << endl;
        cin >> tercih;
        switch (tercih) {
            case 'Y': case 'y':
                cout << "New Department : " ;
                cin.ignore(1000, '\n');
                cin.getline(kayit.dept,15);
                break;
            case 'N': case 'n':
                break;
        }
        defter.guncelle(secim,&kayit);
        cout << "Record has been updated successfully" <<endl;
    }
    getchar();
};
void kayit_sil(){
    char fname[15];
    int secim;
    cout << "Please enter the name of the person you want to delete (press '*' for listing all):" << endl;
    cin.ignore(1000, '\n');
    cin.getline(fname,15);
    int kisisayisi=defter.araAd(fname);
    if(kisisayisi==0){
    cout << "Record can not be found" << endl;
    }
    else {
    if (kisisayisi==1){
    cout << "Record has been found." << endl;
    cout << "Please enter the index of the record if you want to delete this contact (Press -1 to exit without deletion): " ;
    }
    else
    cout << "Please enter the index of the record that you want to delete (Press -1 to exit without deletion): " ;
    cin >> secim;
    if(secim==-1) return;
    defter.sil(secim);
    cout << "Record has been deleted" <<endl;
    }
    getchar();
}

void bastir(){
    cin.ignore(1000, '\n');
    defter.araAd("*");
    getchar();
};
