#include "linkedlist.h"

class ARTraverser
{
    public:
    Node* current;
    virtual bool hasNode() = 0;
    virtual int next() = 0;
};


class ARTF : public ARTraverser
{
public:
    bool hasNode();
    int next();
};

bool ARTF::hasNode()
{
    if (current != NULL)
        return true;
    else
        return false;
}

int ARTF::next()
{
    int temp = current->data;
    current = current->next;
    return temp;
}

class ARTL : public ARTraverser
{
    public:
    bool hasNode();
    int next();
};

bool ARTL::hasNode()
{
    if (current != NULL)
        return true;
    else
        return false;
}

int ARTL::next()
{
    int temp = current->data;
    current=current->prev;
    return temp;
}
