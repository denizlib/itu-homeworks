/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 20.12.2015
*/

#include "deck.h"

using namespace std;

void Stack::create(){
	top = 0;
}
void Stack::close(){
}
bool Stack::push(StackDataType newelement){
	if (top<STACKSIZE){
		element[top++] = newelement;
		return true;
	}
	return false;
}
StackDataType Stack::pop(){
	return element[--top];
}
StackDataType Stack::topStack(){
	return element[top - 1];
}
bool Stack::isempty(){
	return (top == 0);
}
bool Stack::isfull(){
	if (top == STACKSIZE - 1)
		return true;
	return false;
}

void Queue::create(){
	front = 0; back = 0;
}
void Queue::close()
{
}
bool Queue::enqueue(QueueDataType newdata)
{
    if (back<QUEUESIZE)
    {
        qElement[back++] = newdata;
        return true;
    }
    return false;
}
QueueDataType Queue::dequeue()
{
    if (!this->isempty())
        return qElement[front++];
}
bool Queue::isempty()
{
    return (front == back);
}

/*
void Queue::create() {
    front = 0; back = -1;
}

void Queue::close()
{
}
bool Queue::enqueue(QueueDataType newdata)
{
    if ( back != -1 && (front == (back + 1) % QUEUESIZE ) )
        return false;
    else
    {
        cout << "succesfully enqueued" << endl;
        back = (back + 1) % QUEUESIZE;
        qElement[back] = newdata;
        return true;
    }
}

QueueDataType Queue::dequeue()
{
    QueueDataType el = qElement[front];
    if ( front == back )
    {
        front = 0;
        back = -1;
    }
    else front = (front + 1) % QUEUESIZE;

    cout << "dequeued" << endl;
    return el;
}

bool Queue::isempty()
{
    return (front == 0 && back == -1);
}

bool Queue::isfull()
{
    if ((front == back + 1) && (back != -1))
        return true;
    return false;
}*/
