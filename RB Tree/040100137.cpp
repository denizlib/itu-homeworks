#include <iostream>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <stdlib.h>

using namespace std;

int indent_counter;
int woman_counter;
int man_counter;

class Node
{
public:
    string name;  // KEY
    Node *parent;
    Node *left;
    Node *right;
    string color;
    int age;
    string gender;
    int nth_woman;
    int nth_man;
};


class RBTree
{
public:
    Node *root;
    RBTree()
    {
        root = NULL;
    }
    void insert(Node *);
    void fix_insertion(Node *);
    void rotate_left(Node *);
    void rotate_right(Node *);
    void display(Node *, int);
    void inorder();
    void set_nth();
    void traverse(Node *);
    void simple_traverseF(Node *, int);
    void simple_traverseM(Node *, int);
    void find_nth_man(int);
    void find_nth_woman(int);
};

void RBTree::insert(Node * input_from_file)
{
    Node *ptr = new Node;
    Node *traverser;
    Node *input = new Node;
    input->name = input_from_file->name;
    input->age = input_from_file->age;
    input->gender = input_from_file->gender;
    input->right = NULL;
    input->left = NULL;
    input->color = "R";
    traverser = root;
    if (root == NULL)
    {
        root = input;
        input->parent = NULL;
    }
    else
    {
        while (traverser != NULL)
        {
            ptr = traverser;
            if(input->name > traverser->name)
            {
                traverser = traverser->right;
            }
            else
            {
                traverser = traverser->left;
            }
        }
        //cout << ptr->name;
        input->parent = ptr;
        if(input->name > ptr->name)
            ptr->right = input;
        else
            ptr->left = input;
    }
    fix_insertion(input);
}
void RBTree::fix_insertion(Node * input)
{
    if (root == input)
    {
        input->color = "B";
        return;
    }
    else
    {
        while(input != root && input->parent->color == "R")
        {
            if (input->parent->parent->left == input->parent)
            {
                if (input->parent->parent->right != NULL && input->parent->parent->right->color == "R")
                {
                    input->parent->color = "B";
                    input->parent->parent->right->color = "B";
                    input->parent->parent->color = "R";
                    input = input->parent->parent;
                }
                else
                {
                    if (input->parent->right == input)
                    {
                        input = input->parent;
                        rotate_left(input);
                    }
                    input->parent->color = "B";
                    input->parent->parent->color = "R";
                    rotate_right(input->parent->parent);
                }
            }
            else
            {
                if(input->parent->parent->left!=NULL)
                {

                    if(input->parent->parent->left->color=="R")
                    {
                        input->parent->color="B";
                        input->parent->parent->left->color="B";
                        input->parent->parent->color="R";
                        input=input->parent->parent;
                    }
                }
                else
                {
                    if(input->parent->left==input)
                    {
                        input=input->parent;
                        rotate_right(input);
                    }
                    input->parent->color="B";
                    input->parent->parent->color="R";
                    rotate_left(input->parent->parent);
                }
            }
            root->color = "B";
        }
    }
    set_nth();
}
void RBTree::rotate_left(Node *input)
{
    Node *input_right = input->right;
    input->right = input->right->left;
    if (input->right != NULL)
    {
        input->right->parent = input;
    }
    input_right->parent = input->parent;

    if (input->parent == NULL)
    {
        root = input_right;
    }
    else if (input == input->parent->left)
    {
        input->parent->left = input_right;
    }
    else
    {
        input->parent->right = input_right;
    }

    input_right->left = input;
    input->parent = input_right;
    return;
}

void RBTree::rotate_right(Node *input)
{

    Node *input_left = input->left;

    input->left = input_left->right;

    if (input->left != NULL)
    {
        input->left->parent = input;
    }
    input_left->parent = input->parent;

    if (input->parent == NULL)
    {
        root = input_left;
    }
    else if (input == input->parent->left)
    {
        input->parent->left = input_left;
    }
    else
    {
        input->parent->right = input_left;
    }
    input_left->right = input;
    input->parent = input_left;
    return;
}


void RBTree::display(Node *p, int indent)
{
    if(root==NULL)
    {
        cout<<"\nEmpty Tree.";
        return ;
    }
    if (p == NULL)
        return;
    display(p->left, indent+1);
    for (int i = 0; i < indent; ++i)
    {
        cout << "\t";
    }
/*
    if (p->parent != NULL)   //// LINUX
    {
        if (p == p->parent->right)
            cout << "\u2514\u2500\u2500\u2500";
        else if (p == p->parent->left)
            cout << "\u250C\u2500\u2500\u2500";
    }*/

    if (p->parent != NULL)     //// WINDOWS
    {
        if (p == p->parent->right)
            cout << char(192) << char(196)<< char(196)<< char(196);
        else if (p == p->parent->left)
            cout << char(218) << char(196)<< char(196)<< char(196);
    }

    cout << "(" << p->color << ")" << p->name << "-" << p->age << "-" << p->gender << endl;
    display(p->right, indent+1);

}

void RBTree::inorder()
{
    display(root,0);
}

void RBTree::simple_traverseF(Node *p, int n)
{
    if (p == NULL)
        return;
    if (p->nth_woman == n)
    {
        if (n == 1)
            cout << n << "st woman:";
        else if (n == 2)
            cout << n << "nd woman:";
        else if (n == 3)
            cout << n << "rd woman:";
        else if (n>20 && n%10 == 1)
            cout << n << "st woman:";
        else if (n>20 && n%10 == 2)
            cout << n << "nd woman:";
        else if (n>20 && n%10 == 3)
            cout << n << "rd woman:";
        else
            cout << n << "th woman:";
        cout << p->name << endl;
    }
    simple_traverseF(p->left, n);
    simple_traverseF(p->right, n);
}
void RBTree::simple_traverseM(Node *p, int n)
{
    if (p == NULL)
        return;
    if (p->nth_man == n)
    {
        if (n == 1)
            cout << n << "st man:";
        else if (n == 2)
            cout << n << "nd man:";
        else if (n == 3)
            cout << n << "rd man:";
        else if (n>20 && n%10 == 1)
            cout << n << "st man:";
        else if (n>20 && n%10 == 2)
            cout << n << "nd man:";
        else if (n>20 && n%10 == 3)
            cout << n << "rd man:";
        else
            cout << n << "th man:";
        cout << p->name << endl;
    }
    simple_traverseM(p->left, n);
    simple_traverseM(p->right, n);
}

void RBTree::traverse(Node *p)
{
    if (p == NULL)
        return;
    traverse(p->left);
    if (p->gender == "F")
    {
        woman_counter++;
        p->nth_woman = woman_counter;
    }
    else if (p->gender == "M")
    {
        man_counter++;
        p->nth_man = man_counter;
    }
    traverse(p->right);
}

void RBTree::set_nth()
{
    woman_counter = 0;
    man_counter = 0;
    traverse(root);
}

void RBTree::find_nth_man(int n)
{
    simple_traverseM(root, n);
}
void RBTree::find_nth_woman(int n)
{
    simple_traverseF(root, n);
}


void ReadFileIntoTree(RBTree *tree, string filename);

int main(int argc, char* argv[])
{
    //cout << argv[1] << endl;
    RBTree tree;
    ReadFileIntoTree(&tree, argv[1]);
    tree.inorder();
    tree.find_nth_woman(3);
    tree.find_nth_man(4);
    //tree->display(tree->root);
}


void ReadFileIntoTree(RBTree *tree, string filename)
{
    double avg;
    Node* tempNode = new Node;
    int counter = 0;
    ifstream input;
    input.open(filename.c_str());
    string line;
    string word;
    while (getline(input, line))
    {
        istringstream tempLine(line);
        for (int i = 0; i<3; i++)
        {
            getline(tempLine, word, '\t');
            switch(i)
            {
            case 0:
                tempNode->name = word;
                break;
            case 1:
                tempNode->gender = word;
                break;
            case 2:
                tempNode->age = atoi(word.c_str());
                break;
            }
        }
        //cout << tempNode->name << " " << tempNode->gender << " " << tempNode->age << endl;
        tree->insert(tempNode);
        counter++;
    }
    input.close();
}
