/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 22.04.2016
*/

#include <iostream>
#include <stdio.h>

using namespace std;

class Base
{
private:
    friend class HetList;

public:
    Base* next;
    string type;
    int result;
    Base();
    void print();
    virtual void compare(Base*);
};

Base::Base()
{
    type = "Base";
}

void Base::print()
{
    cout << result << " ";
}

class Red : public Base
{
public:
    Red();
    void compare(Base*);
};

Red::Red()
{
    type = "red";
}

class Blue : public Base
{
public:
    Blue();
    void compare(Base*);
};

Blue::Blue()
{
    type = "blue";
}

class DarkRed : public Base
{
public:
    DarkRed();
    void compare(Base*);
};

DarkRed::DarkRed()
{
    type = "darkred";
}

class Green : public Base
{
public:
    Green();
    void compare(Base*);
};

Green::Green()
{
    type = "green";
}



void Base::compare(Base* player2)
{
    //compare type = player2.type
}

void Red::compare(Base* player2)
{
    if (player2->type == "red")
        result = 1;
    if (player2->type == "darkred" || player2->type == "blue")
        result = 0;
    if (player2->type == "green")
        result = 2;
}
void Green::compare(Base* player2)
{
    if (player2->type == "red")
        result = 0;
    if (player2->type == "darkred" || player2->type == "blue")
        result = 2;
    if (player2->type == "green")
        result = 1;;
}
void Blue::compare(Base* player2)
{
    if (player2->type == "red")
        result = 2;
    if (player2->type == "darkred" || player2->type == "green")
        result = 0;
    if (player2->type == "blue")
        result = 1;
}
void DarkRed::compare(Base* player2)
{
    if (player2->type == "green")
        result = 0;
    if (player2->type == "red" || player2->type == "blue")
        result = 2;
    if (player2->type == "darkred")
        result = 1;
}



class HetList    // Heterogeneous linked list
{

public:
    Base *head;
    HetList()
    {
        head=0;
    }
    void insert(Base *);
    void print();
};

void HetList::insert(Base* t)
{
    Base *temp = new Base;
    temp = head;
    if(!head)
    {
        head = t;
        head->next = NULL;
    }
    else
    {
        while (temp -> next)
        {
            temp = temp -> next;
        }
        temp->next = t;
        t->next = NULL;
    }
}


void HetList::print()
{
    Base *tempPtr;
    if (head)
    {
        tempPtr=head;
        while(tempPtr)
        {
            tempPtr->print();
            tempPtr=tempPtr->next;
        }
    }
    else
        cout << "The list is empty" << endl;
}
