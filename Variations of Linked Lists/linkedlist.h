#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

class Node
{
    public:
	int data;
	Node* next=NULL;
	Node* prev=NULL;
};
