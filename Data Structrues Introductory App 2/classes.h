/*
ID: 040100137
Name: Batuhan Denizli
*/

#include <iostream>
#include <string>
#include <string.h>
#include <stdio.h>

using namespace std;

class Airport
{
public:
    string name;
};

class Airline
{
public:
    string name;
};

class Flight
{
public:
    string name;
    int day;
    int month;
    int year;
    string orig;
    string dest;
    string id;
};

class FlightSection
{
public:
    int seatClass;
    bool hasAvailableSeats();
};

class Seat
{
    FlightSection s;
public:
    int row;
    char col;
    int column = col - 'col' +1;
    bool seatStatus[100][10];
};
