#include "cardmanager.h"
#include <fstream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

using namespace std;

void readFileIntoArray(CardManager* Cards,string filename);

int main(int argc, char* argv[])
{
    time_t startSort;
    time_t endSort;
    double seconds;
    string sortType;
    if (argc != 5)
    {
        cerr << "wrong input. enter 4 arguments" << endl;
        return 0;
    }
    CardManager* Cards = new CardManager[2000000];
    readFileIntoArray(Cards, argv[3]);
    int setSize = Cards->curSize;
    string arg1 = argv[1];
    string arg2 = argv[2];
    if (arg1 == "-full")
    {
        if (arg2 == "-i")
        {
            time(&startSort);
            Cards->fullInsertionSort(Cards, setSize);
            time(&endSort);
            seconds = difftime(endSort,startSort);
            sortType = "full insertion";
        }
        else if (arg2 == "-m")
        {
            time(&startSort);
            Cards->fullMergeSort(Cards, 0, setSize-1);
            time(&endSort);
            seconds = difftime(endSort,startSort);
            sortType = "full merge";
        }
    }
    if (arg1 == "-filter")
    {
        if (arg2 == "-i")
        {
            time(&startSort);
            Cards->filterInsertionSort(Cards, setSize);
            time(&endSort);
            seconds = difftime(endSort,startSort);
            sortType = "filter insertion";
        }
        else if (arg2 == "-m")
        {
            time(&startSort);
            Cards->filterMergeSort(Cards, 0, setSize-1);
            time(&endSort);
            seconds = difftime(endSort,startSort);
            sortType = "filter merge";
        }
    }

    ofstream timesFile ("timeTook.txt");
    if (timesFile.is_open())
    {
        cout.precision(6);
        timesFile << sortType  << " for " << arg1 << " "  << arg2 << " took " << fixed << seconds << " seconds" <<  endl;
    }



    ofstream sortedOutputFile (argv[4]);
    if (sortedOutputFile.is_open())
    {
        for (int i=0; i<setSize; i++)
        {
            sortedOutputFile << Cards[i].cardName << "\t" << Cards[i].cardClass << "\t" << Cards[i].cardRarity << "\t" << Cards[i].cardSet << "\t" << Cards[i].cardType << "\t" << Cards[i].cardCost << endl;
        }
        sortedOutputFile.close();
    }
    else cout << "Unable to open file";
    return 0;
}


void readFileIntoArray(CardManager* Cards, string filename)
{
    Cards->curSize=0;
    int lineCounter = 0;
    ifstream input;
    input.open(filename.c_str());
    string line;
    string word;
    while (getline(input, line))
    {
        istringstream tempLine(line);
        for (int i = 0; i<6; i++)
        {
            getline(tempLine, word, '\t');
            switch(i)
            {
            case 0:
                Cards[lineCounter].cardName = word;
                break;
            case 1:
                Cards[lineCounter].cardClass = word;
                break;
            case 2:
                Cards[lineCounter].cardRarity = word;
                break;
            case 3:
                Cards[lineCounter].cardSet = word;
                break;
            case 4:
                Cards[lineCounter].cardType = word;
                break;
            case 5:
                Cards[lineCounter].cardCost = atoi(word.c_str());
                break;
            }
        }
        lineCounter++;
    }
    Cards->curSize=lineCounter;
}



/*        while(tempLine >> word)
        {
            switch(wordCounter)
            {
                case 0:
                    Cards[lineCounter].cardName = word;
                    break;
                case 1:
                    Cards[lineCounter].cardClass = word;
                    break;
                case 2:
                    Cards[lineCounter].cardRarity = word;
                    break;
                case 3:
                    Cards[lineCounter].cardSet = word;
                    break;
                case 4:
                    Cards[lineCounter].cardType = word;
                    break;
                case 5:
                    Cards[lineCounter].cardCost = atoi(word.c_str());
                    break;
            }
            wordCounter++;
        }
        wordCounter = 0;*/
