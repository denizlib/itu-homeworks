/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 13.10.2015
*/
struct book_record
{
     char authorName[20], authorSurname[20], title[20];
     int ISBN, location;
     char bookType[10];
};
