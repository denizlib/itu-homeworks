/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 30.03.2016
*/
#include <iostream>
#include "instructor.h"
#include <stdio.h>
#include <ctype.h>
using namespace std;

class InstructorList
{
private:
    int maxSize;
    int curSize;
    bool full;
public:
    InstructorList();
    InstructorList(int = 100);  // Default is arraySize
    void addInstructor();
    void listAll();
    void deleteInstructor();
    void searchName();
    void searchSurname();
    void searchPhoneNumber();
    void searchRoomNumber();
    Instructor instructorList[100];
};

InstructorList::InstructorList()
{
    Instructor instructorList[100];
    curSize=0;
}

InstructorList::InstructorList(int _arraySize)
{
    Instructor instructorList[_arraySize];
    curSize=0;
}


void InstructorList::addInstructor()
{
    string _title, _fname, _lname, _email, _username;
    char choice;
    choice = 'Y';
    int _pnumber, _rnumber, counter=0;
    string _courses[MAX_COURSES];
    cin.ignore(1000,'\n');
    cout << "Enter the Title" << endl;
    cin >> _title;
    cout << "Enter the First Name" << endl;
    cin >> _fname;
    cout << "Enter the Last Name" << endl;
    cin >> _lname;
    cout << "Enter the Telephone Number" << endl;
    cin >> _pnumber;
    cout << "Enter the Room Number" << endl;
    cin >> _rnumber;
    cout << "Enter the User Name" << endl;
    cin >> _username;
    cout << "Enter the Email" << endl;
    cin >> _email;
    while (choice == 'Y' || choice == 'y')
    {
        cout << "Enter the Course" << endl;
        cin >> _courses[counter++];
        cout << "Do you want to add more courses? (Y/N)" << endl;
        cin >> choice;
    }
    bool duplicate = false;
    for (int i=0; i < curSize ; i++)
    {
        if (_fname == instructorList[i].getFirstName() && _lname == instructorList[i].getLastName())
        {
            cout << "This is a duplicate entry, adding operation failed!" << endl;
            duplicate=true;
            break;
        }
    }
    if (!duplicate)
    {
        instructorList[curSize] = Instructor(_title, _fname, _lname, _pnumber, _rnumber, _username, _email, _courses, counter);
        cout << "Instructor Successfully Added" << endl;
        curSize++;
    }
}

void InstructorList::listAll()
{
    cout << "--------List of all Instructors in Computer Engineering of ITU--------" << endl;
    for (int i = 0; i < curSize ; i++)
    {
        cout << "Title: " << instructorList[i].getTitle() << endl;
        cout << "Name: " << instructorList[i].getFirstName() << endl;
        cout << "Surname: " << instructorList[i].getLastName() << endl;
        cout << "Phone Number: " << instructorList[i].getPhoneNumber() << endl;
        cout << "Room Number: " << instructorList[i].getRoomNumber() << endl;
        cout << "Username: " << instructorList[i].getUsername()<< endl;
        cout << "Email: " << instructorList[i].getEmail() << endl;
        /*cout << "Courses: [";
        for (int i=0; i <curSize ; i++)
            cout << instructorList[i].getCourses() << ",";
        cout << " ]" << endl;*/
        cout << "----------------------------------------------------------------------" << endl;
    }
}

void InstructorList::deleteInstructor()
{
    string name, surname;
    cout << "Enter the name: " << endl;
    cin >> name;
    cout << "Enter the surname: " << endl;
    cin >> surname;
    for (int i=0; i < curSize ; i++)
    {
        if ((name == instructorList[i].getFirstName()) && (surname == instructorList[i].getLastName()))
        {
            for (int j = i; j < curSize; j++)
                instructorList[j] = instructorList[j + 1];
            cout << "Instructor Deleted" << endl;
            curSize--;
        }
    }
}

void InstructorList::searchName()
{
    string name;
    cout << "Enter the name: " << endl;
    cin >> name;
    for (int i=0; i < curSize ; i++)
    {
        if (name == instructorList[i].getFirstName())
        {
            cout << "Title: " << instructorList[i].getTitle() << endl;
            cout << "Name: " << instructorList[i].getFirstName() << endl;
            cout << "Surname: " << instructorList[i].getLastName() << endl;
            cout << "Phone Number: " << instructorList[i].getPhoneNumber() << endl;
            cout << "Room Number: " << instructorList[i].getRoomNumber() << endl;
            cout << "Username: " << instructorList[i].getUsername()<< endl;
            cout << "Email: " << instructorList[i].getEmail() << endl;
            //cout << "Courses: [" << instructorList[i].getCourses() << " ]"<<  endl;
            cout << "----------------------------------------------------------------------" << endl;
        }
    }
}

void InstructorList::searchSurname()
{
    string name;
    cout << "Enter the surname: " << endl;
    cin >> name;
    for (int i=0; i < curSize ; i++)
    {
        if (name == instructorList[i].getLastName())
        {
            cout << "Title: " << instructorList[i].getTitle() << endl;
            cout << "Name: " << instructorList[i].getFirstName() << endl;
            cout << "Surname: " << instructorList[i].getLastName() << endl;
            cout << "Phone Number: " << instructorList[i].getPhoneNumber() << endl;
            cout << "Room Number: " << instructorList[i].getRoomNumber() << endl;
            cout << "Username: " << instructorList[i].getUsername()<< endl;
            cout << "Email: " << instructorList[i].getEmail() << endl;
            //cout << "Courses: [" << instructorList[i].getCourses() << " ]"<<  endl;
            cout << "----------------------------------------------------------------------" << endl;
        }
    }
}

void InstructorList::searchPhoneNumber()
{
    int number;
    cout << "Enter the phone number: " << endl;
    cin >> number;
    for (int i=0; i < curSize ; i++)
    {
        if (number == instructorList[i].getPhoneNumber())
        {
            cout << "Title: " << instructorList[i].getTitle() << endl;
            cout << "Name: " << instructorList[i].getFirstName() << endl;
            cout << "Surname: " << instructorList[i].getLastName() << endl;
            cout << "Phone Number: " << instructorList[i].getPhoneNumber() << endl;
            cout << "Room Number: " << instructorList[i].getRoomNumber() << endl;
            cout << "Username: " << instructorList[i].getUsername()<< endl;
            cout << "Email: " << instructorList[i].getEmail() << endl;
            //cout << "Courses: [" << instructorList[i].getCourses() << " ]"<<  endl;
            cout << "----------------------------------------------------------------------" << endl;
        }
    }
}

void InstructorList::searchRoomNumber()
{
    int number;
    cout << "Enter the room number: " << endl;
    cin >> number;
    for (int i=0; i < curSize ; i++)
    {
        if (number == instructorList[i].getRoomNumber())
        {
            cout << "Title: " << instructorList[i].getTitle() << endl;
            cout << "Name: " << instructorList[i].getFirstName() << endl;
            cout << "Surname: " << instructorList[i].getLastName() << endl;
            cout << "Phone Number: " << instructorList[i].getPhoneNumber() << endl;
            cout << "Room Number: " << instructorList[i].getRoomNumber() << endl;
            cout << "Username: " << instructorList[i].getUsername()<< endl;
            cout << "Email: " << instructorList[i].getEmail() << endl;
            //cout << "Courses: [" << instructorList[i].getCourses() << " ]"<<  endl;
            cout << "----------------------------------------------------------------------" << endl;
        }
    }
}
