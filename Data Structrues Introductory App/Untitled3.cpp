#include <cstring>
#include <stdlib.h>
#include <iomanip>
#include <ctype.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
using namespace std;

struct PhoneNumber
{
	PhoneNumber* next;
	char* number;
	char* name;
	PhoneNumber(char* _name, char* _number)
	{
		name = _name;
		number = _number;
		next = NULL;
	};
	void print()
	{
		cout << "["<< this->name << "*" << this->number << "]";
	};
};
struct Person
 {
	char* name;
	Person* next;
	Person* prev;
	PhoneNumber* phoneNumberHead;
	Person(char* _name)
	{
		name = _name;
		next = NULL;
		phoneNumberHead  = NULL;
	};
	void addPhoneNumber(char* name, char* number)
	{
		PhoneNumber* phoneNumber = new PhoneNumber(name,number);
		if(phoneNumberHead == NULL)
		{
			this->phoneNumberHead = phoneNumber;
			this->phoneNumberHead->next = NULL;
			return;
		}
		PhoneNumber* currentNumber = this->phoneNumberHead;
		while(currentNumber->next != NULL)
		{
			currentNumber = currentNumber->next;
}
		currentNumber->next = phoneNumber;
		phoneNumber->next = NULL;
	};
	void removePhoneNumber(int index) // Assume index > 0
	{
		if(index == 1)
		{
			this->phoneNumberHead = this->phoneNumberHead->next;
			return;
		}
		PhoneNumber* currentNumber = this->phoneNumberHead;
		while(index > 2)
		{
			currentNumber = currentNumber->next;
			index--;
		}
		PhoneNumber* temp = currentNumber->next;
		currentNumber->next = temp->next;
		delete[] temp->name;
		delete temp;
	}
void updatePhoneNumber(int index, char* newValue)
	{
		PhoneNumber* currentNumber = this->phoneNumberHead;
		while(index > 1)
		{
			currentNumber = currentNumber->next;
			index--;
		}
currentNumber->number = newValue;
	}
void deleteAllNumbers()
{
		PhoneNumber* currentNumber = this->phoneNumberHead;
		PhoneNumber* temp;
		while(currentNumber != NULL)
		{
			temp = currentNumber;
			currentNumber = currentNumber->next;
			delete[] temp->name;
			delete temp;
		}
		this->phoneNumberHead = NULL;
}


void list()
{
	cout << "{"<< this->name << "}" << endl;
	int count = 1;
	PhoneNumber* currentNumber = this->phoneNumberHead;
	while(currentNumber != NULL)
{
	cout << count << ": ";
	currentNumber->print();
	cout << endl;
	count++;
	currentNumber = currentNumber->next;
}

}
	void print()
	{
		cout << "{"<< this->name << "}";
		PhoneNumber* currentNumber = this->phoneNumberHead;
		while(currentNumber != NULL)
		{
cout << "---";
			currentNumber->print();
			currentNumber = currentNumber->next;
}
cout << endl;
	};
};
struct PhoneBook
 {
	Person* personHead;


	Person* addPerson(char* name)
	{
		Person* person= new Person(name);
		if(personHead == NULL)
		{
			this->personHead = person;
			this->personHead->next = NULL;
			this->personHead->prev = NULL;
			return person;
		}
		if( strcmp(person->name,personHead->name) <= 0 )
		{
			person->next = this->personHead;
			personHead->prev = person;
			this->personHead = person;
			person->prev = NULL;
			return person;
		}
		Person* currentPerson = this->personHead;
		while(currentPerson->next != NULL && strcmp(person->name,currentPerson->next->name) > 0 )
		{
			currentPerson = currentPerson->next;
		}
		if (currentPerson->next != NULL)
		{
			person->next=currentPerson->next;
			person->prev=currentPerson;
			currentPerson->next->prev = person;
			currentPerson->next=person;
		}
		else
		{
			currentPerson->next = person;
			person->prev = currentPerson;
			person->next = NULL;
		}
		return person;
	};

	void addNumber(char* personName,char* numberType, char* number)
	{
		Person* currentPerson= this->personHead;
		while(currentPerson != NULL && strcmp(personName,currentPerson->name) > 0 )
		{
			currentPerson = currentPerson->next;
		}
		if(currentPerson != NULL && strcmp(personName,currentPerson->name) ==0)
		{
			currentPerson->addPhoneNumber(numberType, number);
		}
		else
		{
			cerr << "The name you entered does not exist." << endl;
		}

	}
	void removePerson(char* personName)
	{

		if(personHead == NULL)
		{
			cerr << "There is no name on the list" << endl;
			return;
		}
		else if (strcmp(personName,personHead->name) < 0)
		{
			cerr << "The name you entered does not exist." << endl;
			return;
		}
		else if (strcmp(personName,personHead->name) == 0)
		{
			Person* temp = this->personHead;
			if(personHead->next != NULL)
			{
				this->personHead=personHead->next;
				personHead->prev = NULL;
			}
			temp->deleteAllNumbers();
			delete[] temp->name;
			delete temp;
			return;
		}
		Person* currentPerson= this->personHead;
		while(currentPerson != NULL && strcmp(personName,currentPerson->name) > 0 )
		{
			currentPerson = currentPerson->next;
		}
		if(currentPerson != NULL && strcmp(personName,currentPerson->name) ==0)
		{
			if(currentPerson->next != NULL)
			{
				currentPerson->prev->next = currentPerson->next;
				currentPerson->next->prev = currentPerson->prev;
			}
			else
			{
				currentPerson->prev->next = NULL;
			}
			currentPerson->deleteAllNumbers();
			delete[] currentPerson->name;
			delete currentPerson;
		}
		else
		{
			cerr << "The name you entered does not exist.(remove)" << endl;
		}
	}
	void removeNumber(char* personName)
	{
		Person* currentPerson= this->personHead;
		while(currentPerson != NULL && strcmp(personName,currentPerson->name) > 0 )
		{
			currentPerson = currentPerson->next;
		}
		if(currentPerson != NULL && strcmp(personName,currentPerson->name) ==0)
		{
			currentPerson->list();
			int number;
			cout << "Choose the index: ";
			cin >> number;
			currentPerson->removePhoneNumber(number);
		}
		else
		{
			cerr << "The name you entered does not exist." << endl;
		}

	}

void updatePerson(char* personName)
{
		Person* currentPerson= this->personHead;
		while(currentPerson != NULL && strcmp(personName,currentPerson->name) > 0 )
		{
			currentPerson = currentPerson->next;
		}
		if(currentPerson != NULL && strcmp(personName,currentPerson->name) ==0)
		{
                char* newName;
                char* nameHolder = currentPerson->name;
                cout << "Enter new name:";
                cin >> newName;

                PhoneNumber* newHead= currentPerson->phoneNumberHead;

                currentPerson->phoneNumberHead = NULL;

                removePerson(nameHolder);
              // removePerson(currentPerson->name);


                Person* newPerson = addPerson(newName);

                newPerson->phoneNumberHead = newHead;

		}
		else
		{
			cerr << "The name you entered does not exist." << endl;
		}

	}

	void updateNumber(char* personName)
	{
		Person* currentPerson= this->personHead;
		while(currentPerson != NULL && strcmp(personName,currentPerson->name) > 0 )
		{
			currentPerson = currentPerson->next;
		}
		if(currentPerson != NULL && strcmp(personName,currentPerson->name) ==0)
		{
			currentPerson->list();
			int number;
			cout << "Choose the index: ";
			cin >> number;
			char* newValue;
			cout << "Enter the new value:";
			cin >> newValue;
			currentPerson->updatePhoneNumber(number,newValue);
		}
		else
		{
			cerr << "The name you entered does not exist." << endl;
		}

	}
	int search(char* personName)
	{
		Person* currentPerson= this->personHead;
		int length = strlen(personName);
		while(currentPerson != NULL && strnicmp(personName,currentPerson->name,length) > 0 )
		{
			currentPerson = currentPerson->next;
		}
		while(currentPerson != NULL && strnicmp(personName,currentPerson->name,length) ==0)
		{
			currentPerson->list();
			currentPerson = currentPerson->next;
		}
		/*else
		{
			cerr << "The name you entered does not exist." << endl;
		}*/

	}
	PhoneBook()
	{
		personHead = NULL;
	};
	void list()
{
		int count = 1;
		Person* currentPerson= this->personHead;
	while(currentPerson != NULL)
{
		cout << count << ": ";
		currentPerson->list();
		cout << endl;
		count++;
		currentPerson= currentPerson->next;
}
}
	void print()
	{
		Person* currentPerson= this->personHead;
		while(currentPerson != NULL)
		{
			currentPerson->print();
			currentPerson = currentPerson->next;
		}
	};
};

int main()
{
	PhoneBook book;
	book.addPerson("denizli");
	book.addPerson("mustafa");
	book.addPerson("atkafasi");
	book.addPerson("alper");
	//book.print();
	book.addNumber("mustafa","ev","213123");
	//book.print();
	book.addNumber("alper","mobil","189325");
	book.addNumber("alper","home","1243");
	book.addNumber("alper","work","884");
	book.list();
	//book.updatePerson("mustafa");
	//book.updateNumber("alper");
	//book.search("a");
	//book.removePerson("mustafa");
      //book.print();
	/*book.removeNumber("alper");
	book.print();
	cout << endl;
	book.removePerson("alper");
	book.print();
	cout << endl;
	book.removePerson("denizli");
	book.print();
	cout << endl;
	book.removePerson("mustafa");
	book.print();
	cout << endl;*/
	return 0;
}















