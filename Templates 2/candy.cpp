/*
    040100137
    Batuhan Denizli
*/

#include "candy.h"

using namespace std;


//Setters

void Candy::setAmount(const float& _amount)
{
    weight=_amount;
}
void Candy::setPrice(const float& _price)
{
    pricePerKG=_price;
}
void Candy::setName(const string& _name)
{
    name=_name;
}

//Getters

const string Candy::getName() const
{
    return name;
}
const float Candy::getAmount() const
{
    return weight;
}
const float Candy::getPrice() const
{
    return pricePerKG;
}

//Constructor


Candy::Candy (string _name, float _amount, float _price)
{
    name=_name;
    weight=_amount;
    pricePerKG=_price;
}

float Candy::getCost()
{
    cost = weight*pricePerKG;
    return cost;
}

