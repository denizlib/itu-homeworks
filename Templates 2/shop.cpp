/*
    040100137
    Batuhan Denizli
*/

#include "shop.h"

using namespace std;

template <class T>
Shop<T>::Shop (const T &objT)     //Copy Constructor
{
        discount=0;
        curSize=1; //first object
        initialSize=32;
        shop = new T[initialSize];
        shop[0].setName(objT.getName());
        shop[0].setAmount(objT.getAmount());
        shop[0].setPrice(objT.getPrice());
        totalCost = shop[0].getCost();
        totalCost *= 1.08;
        //cout << "Cur. Total Cost: " << totalCost << endl;
}
/*
template <class T>
ostream Shop<T>::&operator<<(ostream &output, const Shop &objShop)
*/

template <class T>
void Shop<T>::add(const T &objT)
{
    if(curSize<initialSize)
    {
        shop[curSize].setName(objT.getName());
        shop[curSize].setAmount(objT.getAmount());
        shop[curSize].setPrice(objT.getPrice());
        totalCost += (shop[curSize].getCost())*1.08;
        //cout << "Cur. Total Cost: " << totalCost << endl;
        curSize++;
    }
    else if (curSize==initialSize)
    {
        T * newShop = new T[initialSize*2];      // Dynamically increasing the array
        for (int i=0; i<initialSize; i++)
        {
            newShop[i].setName(shop[i].getName());
            newShop[i].setAmount(shop[i].getAmount());
            newShop[i].setPrice(shop[i].getPrice());
        }
        delete[] shop;
        initialSize*=2;
        shop = newShop;                         //  Reallocated shop array
        shop[curSize].setName(objT.getName());          //Continue adding operation
        shop[curSize].setAmount(objT.getAmount());
        shop[curSize].setPrice(objT.getPrice());
        totalCost += (shop[curSize].getCost())*1.08;
        //cout << "Cur. Total Cost: " << totalCost << endl;
        curSize++;
    }
    else
    {
        cout << "Something is not quite right! (Dynamic Array Size)" << endl;
    }
}

template <class T>
void Shop<T>::setDiscount(const int& _discount)
{
    if ( _discount >= 0 && _discount <=30)
        discount=_discount;
    else
        cerr << "Discount rate is out of range!" << endl;
}

/*
template <class T>
void print()
{
    cout << "**********************\n Number of items:" << curSize << endl;
    for (int i=0; i< curSize; i++)
    {
        cout << i << ": " << shop[i].getName() << " #" << shop[i].getAmount() << " Cost: " << shop[i].getCost() << endl << endl;
    }
    cout << "**********************\n Total Cost: " << totalCost << endl;
    cout << "**********************" << endl;
}
*/



template class Shop<Cookie>;
template class Shop<Icecream>;
template class Shop<Candy>;
