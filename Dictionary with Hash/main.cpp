#include <iostream>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <math.h>
#include <ctime>
#include "dict.h"
#include "char.h"
#include <list>

using namespace std;

// atoi(word.c_str())

void readFileIntoDictionary(Dictionary *dict, string filename);
void readFileIntoList(list<BookChar> *bcList, string filename);
void lookupDict(Dictionary *dict);
void lookupList(list<BookChar> *bclist);

int main()
{
    string file;
    file = "ds-set-input.txt";
    Dictionary dict;
    list<BookChar> bookcharList;
    /*
            DICTIONARY INSERTION
    */
    readFileIntoDictionary(&dict, file);
    /*
            DICTIONARY LOOK UP
    */
    lookupDict(&dict);
    /*
            LIST INSERTION
    */
    readFileIntoList(&bookcharList, file);
    /*
            LIST LOOKUP
    */
    lookupList(&bookcharList);

    return 0;
}

void readFileIntoDictionary(Dictionary *dict, string filename)
{
    cout << "DICTIONARY" << endl;
    double avg;
    BookChar bcobj;
    int counter = 0;
    ifstream input;
    input.open(filename.c_str());
    string line;
    string word;
    double elapsed;
    clock_t end;
    clock_t begin = clock();
    while (getline(input, line))
    {

        if (counter == 1000)
        {
            avg = dict->totalCollusion / 1000.0;
            cout << dict->totalCollusion;
            cout << "Average Number of Collusions (first 1.000) \t\t| " << avg << endl;
        }
        else if (counter == 10000)
        {
            avg = dict->totalCollusion / 10000.0;
            cout << dict->totalCollusion;
            cout << "Average Number of Collusions (first 10.000) \t\t| " << avg << endl;
        }
        else if (counter == 100000)
        {
            avg = dict->totalCollusion / 100000.0;
            cout << dict->totalCollusion;
            cout << "Average Number of Collusions (first 100.000) \t\t| " << avg << endl;
        }
        istringstream tempLine(line);
        for (int i = 0; i<4; i++)
        {
            getline(tempLine, word, '\t');
            switch(i)
            {
            case 0:
                bcobj.pageNumber = atoi(word.c_str());
                break;
            case 1:
                bcobj.lineNumber = atoi(word.c_str());
                break;
            case 2:
                bcobj.index = atoi(word.c_str());
                break;
            case 3:
                bcobj.bChar = word;
                break;
            }
        }
        //cout << bcobj.pageNumber << " " << bcobj.lineNumber << " " << bcobj.index << " " << bcobj.bChar << endl;
        if (bcobj.key() != -1)
            dict->put(bcobj.key(),bcobj);
        counter++;
    }
    cout << dict->totalCollusion;
    avg = dict->totalCollusion / double(counter);
    cout << "Average Number of Collusions (overall) \t\t\t| " << avg << endl;
    end = clock();
    elapsed = double(end - begin) / CLOCKS_PER_SEC;
    cout << endl;
    cout << "Dictionary insertion finished after " << elapsed << " seconds." << endl;
    cout << endl;
    input.close();
}

void readFileIntoList(list<BookChar> *bcList, string filename)
{
    cout << "LIST" << endl;
    BookChar bcobj;
    int counter = 0;
    list<BookChar>::iterator bcIterator = bcList->begin();
    ifstream input;
    input.open(filename.c_str());
    string line;
    string word;
    double elapsed;
    clock_t end;
    clock_t begin = clock();
    while (getline(input, line))
    {
        istringstream tempLine(line);
        for (int i = 0; i<4; i++)
        {
            getline(tempLine, word, '\t');
            switch(i)
            {
            case 0:
                bcobj.pageNumber = atoi(word.c_str());
                break;
            case 1:
                bcobj.lineNumber = atoi(word.c_str());
                break;
            case 2:
                bcobj.index = atoi(word.c_str());
                break;
            case 3:
                bcobj.bChar = word;
                break;
            }
        }
        bcList->insert(bcIterator, bcobj);
        //cout << "back key: " << bcList.back().key() << endl;
        //cout << "iterator: " << *bcIterator.key() << endl;
        bcIterator++;
        counter++;
    }
    end = clock();
    elapsed = double(end - begin) / CLOCKS_PER_SEC;
    cout << endl;
    cout << "List insertion finished after " << elapsed << " seconds." << endl;
    cout << endl;
    input.close();
}

void lookupDict(Dictionary *dict)
{
    BookChar bcobj;
    int counter = 0;
    ifstream input;
    ofstream output("ds-set-output-dict.txt",ios::out);
    input.open("ds-set-lookup.txt");
    string line;
    string word;
    double elapsed;
    clock_t end;
    clock_t begin = clock();
    while (getline(input, line))
    {
        istringstream tempLine(line);
        for (int i = 0; i<3; i++)
        {
            getline(tempLine, word, '\t');
            switch(i)
            {
            case 0:
                bcobj.pageNumber = atoi(word.c_str());
                break;
            case 1:
                bcobj.lineNumber = atoi(word.c_str());
                break;
            case 2:
                bcobj.index = atoi(word.c_str());
                break;
            }
        }
        bcobj=dict->get(bcobj.key());
        if (bcobj.pageNumber != 0 || bcobj.key() != -1 )
        {
            stringstream s;
            s << bcobj.pageNumber << "\t" << bcobj.lineNumber << "\t" <<  bcobj.index << "\t" << bcobj.bChar;
            output << s.str() << endl;
        }
        counter++;
    }
    end = clock();
    elapsed = double(end - begin) / CLOCKS_PER_SEC;
    cout << "Dictionary lookup finished after " << elapsed << " seconds." << endl;
    cout << endl;
    cout << "------------------------------------------------------------------" << endl;
    input.close();
    output.close();
}


void lookupList(list<BookChar> *bcList)
{
    list<BookChar>::iterator bcIterator;
    BookChar bcobj;
    int counter = 0;
    ifstream input;
    ofstream output("ds-set-output-list.txt",ios::out);
    input.open("ds-set-lookup.txt");
    string line;
    string word;
    double elapsed;
    clock_t end;
    clock_t begin = clock();
    while (getline(input, line))
    {
        istringstream tempLine(line);
        for (int i = 0; i<3; i++)
        {
            getline(tempLine, word, '\t');
            switch(i)
            {
            case 0:
                bcobj.pageNumber = atoi(word.c_str());
                break;
            case 1:
                bcobj.lineNumber = atoi(word.c_str());
                break;
            case 2:
                bcobj.index = atoi(word.c_str());
                break;
            }
        }
        for (bcIterator = bcList->begin(); bcIterator != bcList->end(); bcIterator++)
        {
            if (bcobj.key() == (*bcIterator).key())
            {
                bcobj.bChar = (*bcIterator).bChar;
                stringstream s;
                s << bcobj.pageNumber << "\t" << bcobj.lineNumber << "\t" <<  bcobj.index << "\t" << bcobj.bChar;
                output << s.str() << endl;
            }
        }
        counter++;
    }
    end = clock();
    elapsed = double(end - begin) / CLOCKS_PER_SEC;
    cout << "List lookup finished after " << elapsed << " seconds." << endl;
    cout << endl;
    cout << "------------------------------------------------------------------" << endl;
    input.close();
    output.close();
}
