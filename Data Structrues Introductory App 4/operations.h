/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 13.10.2015
*/
#ifndef OPERATIONS_H
#define OPERATIONS_H
#include <stdio.h>
#include "record.h"
struct Library
{
    char *filename;
    FILE *library;
    void create();
    void close();
    void add(book_record *);
    int searchAuthor(char []);
    int searchType(char []);
    int searchISBN(int);
    void removeBook(int kayitno);
    void update(int kayitno, book_record *);
    void printAll();
};
#endif

