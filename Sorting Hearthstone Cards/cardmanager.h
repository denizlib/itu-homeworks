#include <iostream>

using namespace std;

class CardManager
{
    public:
    string cardName, cardClass, cardRarity, cardSet, cardType;
    int cardCost;
    int curSize;
    void fullInsertionSort(CardManager*, int);
    void filterInsertionSort(CardManager*, int);
    void fullMergeSort(CardManager*, int, int);
    void filterMergeSort(CardManager*, int, int);
};
