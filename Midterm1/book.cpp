#include <iostream>
#include "book.h"

Book::Book(const string& _title,const string& _author, const int _isbn)
{
    title = _title;
    author = _author;
    isbn = _isbn;
}

void Book::print () const
{
    cout << title << endl << author << endl << isbn << endl;
}

int Book::getISBN() const
{
    return isbn;
}

string Book::getAuthor() const
{
    return author;
}

string Book::getTitle() const
{
    return title;
}

void Book::setISBN(int _isbn)
{
    isbn = _isbn;
}

void Book::setAuthor(const string& _author)
{
    author = _author;
}

void Book::setTitle(const string& _title)
{
    title = _title;
}
