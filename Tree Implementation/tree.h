struct node {
	int number;
	node *left;
	node *right;
};
struct tree {
	node *root;
};
