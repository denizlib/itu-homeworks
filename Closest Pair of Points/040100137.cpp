#include <stdio.h>
#include <string>
#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <sstream>
#include <fstream>
#include <time.h>
#include <sys/time.h>
#include <iostream>
#include <algorithm>

using namespace std;

int totalDistanceCalculation;

class Ball
{
public:
    int x, y, z;
};

int readFirstLine(string filename)
{
    int totalSize = 0;
    int lineCounter = 0;
    ifstream input;
    input.open(filename.c_str());
    string line;
    string word;
    while (getline(input, line))
    {
        istringstream tempLine(line);
        if (lineCounter == 0)
        {
            getline(tempLine, word, ' ');
            totalSize = atoi(word.c_str());
            input.close();
            return totalSize;
        }
        else
            break;
        lineCounter++;
    }
    input.close();
    return 0;
}

void readFileIntoArray(Ball* Balls, string filename)
{
    int wordCounter = 0;
    int lineCounter = 0;
    ifstream input;
    input.open(filename.c_str());
    string line;
    string word;
    while (getline(input, line))
    {
        istringstream tempLine(line);
        wordCounter = 0;
        while(tempLine >> word)
        {
            if (lineCounter == 0)
                break;
            switch(wordCounter)
            {
            case 0:
                Balls[lineCounter-1].x = atoi(word.c_str());
                break;
            case 1:
                Balls[lineCounter-1].y = atoi(word.c_str());
                break;
            case 2:
                Balls[lineCounter-1].z = atoi(word.c_str());
                break;
            }
            wordCounter++;
        }
        lineCounter++;
    }
    input.close();
}



bool compareX(const Ball &lhs, const Ball &rhs)  // Custom compare functions for std::sort()
{
    return lhs.x < rhs.x;
}

bool compareY(const Ball &lhs, const Ball &rhs)  // Custom compare functions for std::sort()
{
    return lhs.y < rhs.y;
}

float euclidianDist(Ball b1, Ball b2)
{
    totalDistanceCalculation++;
    return sqrt( (b1.x - b2.x)*(b1.x - b2.x) +
                 (b1.y - b2.y)*(b1.y - b2.y) +
                 (b1.z - b2.z)*(b1.z - b2.z)
               );
}

float bruteForce(Ball B[], int n)
{
    float min = FLT_MAX;
    for (int i = 0; i < n; ++i)
        for (int j = i+1; j < n; ++j)
            if (euclidianDist(B[i], B[j]) < min)
                min = euclidianDist(B[i], B[j]);
    return min;
}

float min(float x, float y)
{
    if (x < y)
        return x;
    else
        return y;
}



float midSectionClosest(Ball midSection[], int size, float d)
{
    float min = d;  // Initialize the minimum distance as d

    sort(midSection, midSection+size, compareY);

    for (int i = 0; i < size; ++i)
        for (int j = i+1; j < size && (midSection[j].y - midSection[i].y) < min; ++j)
            if (euclidianDist(midSection[i],midSection[j]) < min)
                min = euclidianDist(midSection[i], midSection[j]);

    return min;
}

float closest_recursive(Ball B[], int n)
{
    if (n < 4) // BruteForce if there are less than 4 points in the array
        return bruteForce(B, n);
    int mid = n/2;
    Ball midPoint = B[mid];


    float distLeft = closest_recursive(B, mid);
    float distRight = closest_recursive(B + mid, n-mid);

    float d = min(distLeft, distRight); // Upper bound for the middle section.

    Ball midSection[n];
    int j = 0;
    for (int i = 0; i < n; i++)
        if (abs(B[i].x - midPoint.x) < d)
            midSection[j] = B[i], j++;
    return min(d, midSectionClosest(midSection, j, d) );
}

float closest(Ball B[], int n)
{
    clock_t t;
    totalDistanceCalculation = 0;
    t = clock();
    sort(B, B+n, compareX); // Sort by x before starting the recursive operation
    t = clock() - t;
    cout << "Sorting by X took " << ((float)t)/CLOCKS_PER_SEC << " seconds in total." << endl;
    return closest_recursive(B, n);
}

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        cerr << "wrong input. enter 1 argument" << endl;
        return 0;
    }
    string arg1 = argv[1];
    string file;
    file = arg1;
    Ball * ballsArray;
    int sizeofArray = readFirstLine(file);
    ballsArray = new Ball[sizeofArray];
    readFileIntoArray(ballsArray, file);
    clock_t t;
    t = clock();
    cout << "The distance is " << closest(ballsArray, sizeofArray) << endl;
    t = clock() - t;
    cout << "Calculation took " << ((float)t)/CLOCKS_PER_SEC << " seconds in total." << endl;
    cout << "Number of total distance calculations is " << totalDistanceCalculation << endl;
    return 0;

}
