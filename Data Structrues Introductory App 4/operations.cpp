/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 13.10.2015
*/
#include "operations.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>
using namespace std;
int sayac=0;
void Library::add(book_record *ykptr)
{
    fseek(library, 0, SEEK_END);
    fwrite(ykptr, sizeof(book_record), 1, library);
}
void Library::create()
{
    filename = "library.txt";
    library = fopen( filename, "r+" );
    if(!library)
    {
        if(!(library = fopen( filename, "w+" )))
        {
            cerr << "File can not be opened" << endl;
            exit(1);
        }
    }
}
void Library::close()
{
    fclose(library);
}
int Library::searchAuthor(char aranacak[])
{
    sayac=0;
    book_record k;
    bool tumu=false;
    int bulunan=0;
    if(strcmp(aranacak,"*")==0)
        tumu=true;
    fseek(library, 0, SEEK_SET);
    while(!feof(library))
    {
        sayac++;
        fread( &k, sizeof (book_record), 1, library);
        if(feof(library)) break;
        if(!tumu && strnicmp(k.authorName, aranacak, strlen(aranacak))!=0)
            continue;
        cout << sayac << "." << k.authorName << "   " << k.authorSurname << "   " << k.title << "   " << k.bookType << "    " << k.ISBN << "    " << k.location  << endl;
        bulunan++;
    }
    return bulunan;
}
int Library::searchType(char aranacak[])
{
    book_record k;
    sayac=0;
    int flag=0;
    bool tumu=false;
    int bulunan=0;
    if(strcmp(aranacak,"*")==0)
        tumu=true;
    fseek(library, 0, SEEK_SET);
    while(!feof(library))
    {
        sayac++;
        fread( &k, sizeof (book_record), 1, library);
        if(feof(library)) break;
        if(!tumu && strnicmp(k.bookType, aranacak, strlen(aranacak))!=0)
            continue;
        if (flag==0)
        {
            cout << "You searched for this type of books: " << k.bookType << endl;
            flag++;
        }
        cout << sayac << "." << k.authorName << "   " << k.authorSurname << "   " << k.title << "   " << k.bookType << "    " << k.ISBN << "    " << k.location  << endl;
        bulunan++;
    }
    return bulunan;
}
int Library::searchISBN(int aranacak)
{
    sayac=0;
    book_record k;
    int amountFound=0;
    fseek(library, 0, SEEK_SET);
    while(!feof(library))
    {
        sayac++;
        fread( &k, sizeof (book_record), 1, library);
        if(feof(library)) break;
        if(k.ISBN == aranacak)
        {
            cout << sayac << "." << k.authorName << "   " << k.authorSurname << "   " << k.title << "   " << k.bookType << "    " << k.ISBN << "    " << k.location  << endl;
            amountFound++;
            break;
        }
    }
    return amountFound;
}
void Library::update(int kayitno, book_record *ykptr)
{
    if(fseek(library, sizeof(book_record)*(kayitno-1), SEEK_SET)==0)
        fwrite(ykptr, sizeof(book_record), 1, library);
}
void Library::removeBook(int kayitno)
{
    book_record emptyRecord= {"","","",0,0,""};
    if(fseek(library, sizeof(book_record)*(kayitno-1), SEEK_SET)==0)
    {
        fwrite(&emptyRecord, sizeof(book_record), 1, library);
    }
}
void Library::printAll()
{
    book_record k;
    sayac=0;
    fseek(library, 0, SEEK_SET);
    while(!feof(library))
    {
        if (sayac == 0 )
        {
            cout << "Name\tSurname  Title\tType\tISBN\tLocation" << endl;
        }
        sayac++;
        fread( &k, sizeof (book_record), 1, library);
        if(feof(library)) break;
        cout << sayac << "." << k.authorName << "   " << k.authorSurname << "   " << k.title << "   " << k.bookType << "    " << k.ISBN << "    " << k.location  << endl;
    }
}
