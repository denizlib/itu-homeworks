#include "dict.h"

Dictionary::Dictionary()
{
    collusionAmount = 0;
    totalCollusion = 0;
    for (int i=0; i<M; i++)
    {
        HashTable[i] = new HashNode;
        HashTable[i]->key = -1;
        //HashTable[i]->value = nullptr;
    }
}

unsigned long Dictionary::h (unsigned long x)
{
    return floor(M * fmod((x * A),1));
}

int Dictionary::put(int k, BookChar v)
{
    collusionAmount = 0;
    int index = h(k);
    if (HashTable[index]->key == -1)
    {
        HashTable[index]->key = k;
        HashTable[index]->value = v;
    }
    else
    {
        while(HashTable[index]->key != -1 && HashTable[index]->key != k)
        {
            /*
                PROBING FUNCTION
            */
            index = (index + 7*collusionAmount + 3 * collusionAmount^2)%M;
            collusionAmount++;
            totalCollusion++;
        }
        HashTable[index]->key = k;
        HashTable[index]->value = v;
    }
}

void Dictionary::printTable()
{
    for (int i = 0; i< M;i++)
    {
        cout << "Key: " << HashTable[i]->key << " Pn: " << HashTable[i]->value.pageNumber << " Ln: " << HashTable[i]->value.lineNumber << " In: " << HashTable[i]->value.index << " Bc: " << HashTable[i]->value.bChar << endl;
    }
}

BookChar Dictionary::get(int k)
{
    collusionAmount = 0;
    int index = h(k);
    if (HashTable[index]->key == k)
    {
        return HashTable[index]->value;
    }
    else
    {
        while (HashTable[index]->key != k && HashTable[index]->key != -1)
        {
            //cout << "key " <<  HashTable[index]->key << "k:" << k << endl;
            index = (index + 7*collusionAmount + 3 * collusionAmount^2)%M;
            collusionAmount++;
        }
        return HashTable[index]->value;
    }
}
