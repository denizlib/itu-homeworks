/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 08.05.2016
*/
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>

using namespace std;

class Toy
{
    public:
    string label;
    float weight, length, width;
    bool hasBatteries;

    void setContainsBattery(bool battery)
    {
        hasBatteries=battery;
    }
    Toy(string _label, float _weight, float _length, float _width, bool _hasBatteries)
    {
        label=_label;
        weight=_weight;
        length=_length;
        width=_width;
        hasBatteries=_hasBatteries;
    }
    Toy()
    {

    }
    ~Toy(){}
    friend std::ostream& operator<< (std::ostream& stream, const Toy& _box)
    {
        if (_box.hasBatteries==true)
            stream << "Toy Label: " << _box.label << " # " << _box.length << "x" << _box.width << " " << _box.weight << "kg Contains Battery";
        else
            stream << "Toy Label: " << _box.label << " # " << _box.length << "x" << _box.width << " " << _box.weight << "kg No Battery";
        return stream;
    }
};
