#ifndef CHR
#define CHR
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <math.h>
using namespace std;

class BookChar
{
public:
    BookChar();
    int pageNumber;
    int lineNumber;
    int index;
    string bChar;
    long int key();
};

#endif

