/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 04.05.2015
*/
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <sstream>
using namespace std;
#ifndef CRUISE_H_
#define CRUISE_H_

class Cruise
{
    private:
        string journey;
    public:
        double cost;
        Cruise()
        {

        };
        Cruise(const char* _journey, double _cost)
        {
            journey = string(_journey);
            cost = _cost;
        }

        void setCost(double _cost)
        {
            cost = _cost;
        }

        friend ostream &operator<<( ostream &output, const Cruise &C)
        {
            output << "Cruise Name: " << C.journey << " Cost: " << C.cost;
            return output;
        }
};



#endif /* CRUISE_H_ */
