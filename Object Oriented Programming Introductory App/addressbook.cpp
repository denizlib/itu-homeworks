/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 30.03.2016
*/
#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <ctype.h>
#include "instructorlist.h"

using namespace std;

void main_menu();
bool do_operations(char);

int main()
{
    InstructorList theList(15);
    bool endProgram=false;
    char secim;
    cin.ignore(1000, '\n');
    while (!endProgram)
    {
        main_menu();
        cin >> secim;
        switch (secim)
        {
            case '1':
                theList.addInstructor();
                cout << "Do you want to perform another operation? (Y/N):";
                cin >> secim;
                switch (secim)
                {
                case 'Y': case 'y':
                    break;
                default:
                    endProgram=true;
                }
                break;
            case '2':
                theList.deleteInstructor();
                cout << "Do you want to perform another operation? (Y/N):";
                cin >> secim;
                switch (secim)
                {
                case 'Y': case 'y':
                    break;
                default:
                    endProgram=true;
                }
                break;
            case '3':
                theList.listAll();
                cout << "Do you want to perform another operation? (Y/N):";
                cin >> secim;
                switch (secim)
                {
                case 'Y': case 'y':
                    break;
                default:
                    endProgram=true;
                }
                break;
            case '4':
                theList.searchName();
                cout << "Do you want to perform another operation? (Y/N):";
                cin >> secim;
                switch (secim)
                {
                case 'Y': case 'y':
                    break;
                default:
                    endProgram=true;
                }
                break;
            case '5':
                theList.searchSurname();
                cout << "Do you want to perform another operation? (Y/N):";
                cin >> secim;
                switch (secim)
                {
                case 'Y': case 'y':
                    break;
                default:
                    endProgram=true;
                }
                break;
            case '6':
                theList.searchPhoneNumber();
                cout << "Do you want to perform another operation? (Y/N):";
                cin >> secim;
                switch (secim)
                {
                case 'Y': case 'y':
                    break;
                default:
                    endProgram=true;
                }
                break;
            case '7':
                theList.searchRoomNumber();
                cout << "Do you want to perform another operation? (Y/N):";
                cin >> secim;
                switch (secim)
                {
                case 'Y': case 'y':
                    break;
                default:
                    endProgram=true;
                }
                break;
            case '8':
                cout << "Are you sure that you want to terminate the program? (Y/N):";
                cin >> secim;
                if(secim=='Y' || secim=='y')
                    endProgram=true;
                break;
            default:
                cout << "-------Error: You have made an invalid choice--------" << endl;
                break;
        }
    }
    return EXIT_SUCCESS;
}
void main_menu()
{
    cout << "Please select the operation to perform and enter operation code" << endl <<
    "1. Add a new instructor" << endl <<
    "2. Delete an instructor" << endl <<
    "3. List all instructors" << endl <<
    "4. Search by Name" << endl <<
    "5. Search by Surname" << endl <<
    "6. Search by Telephone Number" << endl <<
    "7. Search by Room Number" << endl <<
    "8. Exit" << endl;
    cout << "Your selection is: " << endl;

}
