/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 18.09.2014
*/
#ifndef DOSYAISLEMLERI_H
#define DOSYAISLEMLERI_H
#include <stdio.h>
#include "record.h"

struct Dosya{
    char *dosyaadi;
    FILE *facultyrecord;
    void olustur();
    void kapat();
    void ekle(Faculty_Record *);
    int araDept(char[]);
    int araAd(char[]);
    void guncelle(int kayitno, Faculty_Record *);
    void sil(int kayitno);
    int maintenance();
    Faculty_Record getir(int kayitno);
};
#endif
