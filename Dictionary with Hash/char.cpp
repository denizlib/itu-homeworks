#include "char.h"

using namespace std;

BookChar::BookChar()
{
    pageNumber = 0;
}

long int BookChar::key()
{
    stringstream s;
    if (lineNumber > 0 && index > 0)
    {
        if (index >= 10 && lineNumber < 10)
            s << pageNumber << "0" << lineNumber <<  index;
        if (index < 10 && lineNumber >= 10)
            s << pageNumber << lineNumber << "0" << index;
        if (index < 10 && lineNumber < 10)
            s << pageNumber << "0" << lineNumber << "0" << index;
        if (index >= 10 && lineNumber >= 10)
            s << pageNumber << lineNumber << index;
        return  atoi( s.str().c_str());
    }
    return -1;
}
