/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 22.04.2016
*/

#include "cards.h"
#include <fstream>
#include <ctype.h>

using namespace std;
int totalLoss=0, totalWin=0, totalTie=0;
void display(HetList&, HetList&);

int main()
{
    HetList theHetList1;
    HetList theHetList2;
    int flag = 0;
    char ch;
    fstream infile("deck.txt", fstream::in);;
    cout << "Player1: ";
    while (infile >> noskipws >> ch)
    {
        if (ch == '\n')
        {
            cout << endl;
            flag = 1;
            cout << "Player2: ";
        }
        if (flag == 0 && !isspace(ch))
        {
            if (ch=='R')
            {
                cout << ch << " ";
                Red* obj = new Red;
                theHetList1.insert(obj);
            }
            else if (ch=='D')
            {
                cout << ch << " ";
                DarkRed* obj = new DarkRed;
                theHetList1.insert(obj);
            }
            else if (ch=='G')
            {
                cout << ch << " ";
                Green* obj = new Green;
                theHetList1.insert(obj);
            }
            else if (ch=='B')
            {
                cout << ch << " ";
                Blue* obj = new Blue;
                theHetList1.insert(obj);
            }
            else
            {
                cout << "Error! Wrong type of card is entered! Terminating..." << endl;
                return 67;
            }
        }
        else if (flag == 1 && !isspace(ch))
        {
            if (ch=='R')
            {
                cout << ch << " ";
                Red* obj = new Red;
                theHetList2.insert(obj);
            }
            else if (ch=='D')
            {
                cout << ch << " ";
                DarkRed* obj = new DarkRed;
                theHetList2.insert(obj);
            }
            else if (ch=='G')
            {
                cout << ch << " ";
                Green* obj = new Green;
                theHetList2.insert(obj);
            }
            else if (ch=='B')
            {
                cout << ch << " ";
                Blue* obj = new Blue;
                theHetList2.insert(obj);
            }
            else
            {
                cout << "Error! Wrong type of card is entered! Terminating..." << endl;
                return 67;
            }
        }
    }
    cout << endl;
    infile.close();
    Base *tempPtr1 = theHetList1.head;
    Base *tempPtr2 = theHetList2.head;
    while (tempPtr1)
    {
        tempPtr1->compare(tempPtr2);
        tempPtr2->result = 2-tempPtr1->result;
        if (tempPtr1->result == 0)
            totalLoss++;
        if (tempPtr1->result == 1)
            totalTie++;
        if (tempPtr1->result == 2)
            totalWin++;
        tempPtr1=tempPtr1->next;
        tempPtr2=tempPtr2->next;
    }
    /*tempPtr1 = theHetList1.head;
    tempPtr2 = theHetList2.head;
    while (tempPtr1->next)
    {
        tempPtr2->compare(tempPtr1);
        tempPtr1=tempPtr1->next;
        tempPtr2=tempPtr2->next;
    }
    tempPtr2->compare(tempPtr1);*/
    /*cout << "Score 1: ";
    theHetList1.print();
    cout << endl;
    cout << "Score 2: ";
    theHetList2.print();
    cout << endl;
    cout << "Total Score1: Lose: " << totalLoss << " Tie: " << totalTie << " Win: " << totalWin << endl;
    cout << "Total Score2: Lose: " << totalWin << " Tie: " << totalTie << " Win: " << totalLoss << endl;*/

    display(theHetList1, theHetList2);

    char choice = 'r';
    while (choice == 'r' || choice == 'R')
    {
        cout << "Press (R) again to show results. Press any key to exit." << endl;
        cin>> choice;
        if (choice == 'r' || choice == 'R')
        {
            display(theHetList1, theHetList2);
        }
        else
            cout << "Terminating...";
    }
    return 0;
}

void display(HetList& theHetList1, HetList& theHetList2)
{
    cout << "Score1: ";
    theHetList1.print();
    cout << endl;
    cout << "Score2: ";
    theHetList2.print();
    cout << endl;
    cout << "Total Score1: Lose: " << totalLoss << " Tie: " << totalTie << " Win: " << totalWin << endl;
    cout << "Total Score2: Lose: " << totalWin << " Tie: " << totalTie << " Win: " << totalLoss << endl;
    if (totalWin>totalLoss)
        cout << "Winner: Player1" << endl;
    if (totalWin<totalLoss)
        cout << "Winner: Player2" << endl;
    if (totalWin==totalLoss)
        cout << "It's a TIE!" << endl;
}








