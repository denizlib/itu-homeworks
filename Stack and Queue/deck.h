/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 20.12.2015
*/


#ifndef DECK_H
#define DECK_H

#include <iostream>

#define STACKSIZE 1000
#define QUEUESIZE 1000
#define DECKSIZE 52



struct Card
{
    //enum CardType{ Sinek, Karo, Kupa, Maca  };
    int cardValue; // values later get converted to actual name
    char cardType;
};

typedef Card StackDataType;

struct Stack{
	StackDataType element[STACKSIZE];
	int top;
	void create();
	void close();
	bool push(StackDataType);
	StackDataType pop();
	StackDataType topStack();
	bool isempty();
	bool isfull();
};

typedef Card QueueDataType;

struct Queue{
	QueueDataType qElement[QUEUESIZE];
	int front;
	int back;
	int counter;
	void create();
	void close();
	bool enqueue(QueueDataType);
	QueueDataType dequeue();
	bool isempty();
	bool isfull();
};


struct Deck : Queue
{
    int random;
    Card cardArray[DECKSIZE];
    void create();
    void shuffle();
};

#endif
