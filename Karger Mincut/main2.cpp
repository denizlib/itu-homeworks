#include <time.h>
#include <stdio.h>
#include <string>
#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <sstream>
#include <fstream>
#include <time.h>
#include <sys/time.h>
#include <iostream>
#include <algorithm>

using namespace std;

bool specialCase;

class Edge
{
public:
    int src;
    int dest;
};

class Graph
{
public:
    int V;
    int E;
    Edge* edge;
};

class subset
{
public:
    int parent;
    int rank;
};
int cutVertices(int * a, int size);
int findPopular(int*a,int size);
int find(subset subsets[], int i);
void Union(subset subsets[], int x, int y);

int kargerMinCut(Graph* graph)
{
    int edgesToCut[1000];
    int V = graph->V;
    int E = graph->E;
    Edge *edge = graph->edge;

    subset *subsets = new subset[V];

    for (int v = 0; v < V; ++v)
    {
        subsets[v].parent = v;
        subsets[v].rank = 0;
    }

    int vertices = V;
    if (V==5) specialCase = true;
    int tempsrc;
    int tempdest;
    int cutedges = 0;
    while (vertices > 2)
    {

        int i = rand() % E;
        tempdest =  edge[i].dest;
        tempsrc = edge[i].src;
        int subset1 = find(subsets, edge[i].src);
        int subset2 = find(subsets, edge[i].dest);

        if (subset1 == subset2)
            continue;

        else
        {
            //printf("Contracting edge %d-%d\n", tempsrc+1, tempdest+1);
            vertices--;
            Union(subsets, subset1, subset2);
        }
    }

    for (int i=0; i<E; i++)
    {
        //cout << "Subset " << i << ": " << subsets[i].parent << " " << subsets[i].rank << endl;
        int subset1 = find(subsets, edge[i].src);
        int subset2 = find(subsets, edge[i].dest);
        if (subset1 != subset2)
        {
            edgesToCut[2*cutedges] = edge[i].src;
            edgesToCut[2*cutedges+1] = edge[i].dest;
            cutedges++;
            //printf("Edges that are cut %d-%d\n", edge[i].src+1, edge[i].dest+1);
        }
    }
    return cutVertices(edgesToCut,cutedges*2);
}

int find(subset* subsets, int i)
{
    int subsetparent = subsets[i].parent;
    if (subsets[i].parent != i)
        subsets[i].parent = find(subsets, subsets[i].parent);

    return subsets[i].parent;
}

void Union(subset subsets[], int x, int y)
{
    int xroot = find(subsets, x);
    int yroot = find(subsets, y);

    if (subsets[xroot].rank < subsets[yroot].rank)
        subsets[xroot].parent = yroot;
    else if (subsets[xroot].rank > subsets[yroot].rank)
        subsets[yroot].parent = xroot;

    else
    {
        subsets[yroot].parent = xroot;
        subsets[xroot].rank++;
    }
}

void initGraph(Graph* graph, int V, int E)
{
    graph->V = V;
    graph->E = E;
    graph->edge = new Edge[E];
}

void readFileIntoArray(Graph* graph, string filename)
{
    int v,e;
    int lineCounter = 0;
    int wordCounter = 0;
    ifstream input;
    input.open(filename.c_str());
    string line;
    string word;
    while (getline(input, line))
    {
        istringstream tempLine(line);
        if (lineCounter == 0)
        {
            wordCounter=0;
            while(tempLine >> word)
            {
                if (!wordCounter) v = atoi(word.c_str());
                else e = atoi(word.c_str());
                wordCounter++;
                //cout << word << " ";
            }
            initGraph(graph,v,e);
            //cout << endl;
        }
        else if (lineCounter <= e+1)
        {
            wordCounter=0;
            while(tempLine >> word)
            {
                if (!wordCounter) graph->edge[lineCounter-1].src = atoi(word.c_str())-1;
                else graph->edge[lineCounter-1].dest = atoi(word.c_str())-1;
                wordCounter++;
            }
        }
        lineCounter++;
    }
}

bool isSingleVertice(int * a, int size, int v)
{
    int count = 0;
    for (int i = 0; i < size; i=i+2)
    {
        if (a[i] == v || a[i+1] == v)
            count++;
    }
    if (count == size/2)
    {
        return true;
    }
    else
        return false;
}

int findPopular(int * a, int size)
{
    int max_count = 0;
    int popular = 0;
    for (int i=0; i<size; i++)
    {
        //cout << a[i] << endl;
        int temp = a[i];
        int count=1;
        for (int j=i+1; j<size; j++)
        {
            if (a[i]==a[j])
                count++;
        }
        if (a[i] == -1)
            count = 0;

        if (count>max_count)
        {
            max_count = count;
            popular = a[i];
        }
    }
    return popular;
}

int cutVertices(int * a, int size)
{
    int cutVertices=0;
    int remaining = size/2;
    int popular = findPopular(a,size);
    //cout << "----" << endl;
    //cout << "popular " << popular+1 << endl;
    if(isSingleVertice(a,size,popular))
    {
        if (specialCase)
            return 1;
        else return INT_MAX;
    }
    else
    {
        while (remaining > 0)
        {
            popular = findPopular(a,size);
            for (int i = 0; i < size; i=i+2)
            {
                //cout << "remaining " << a[i]+1 << " " << a[i+1]+1 << endl;
                //cout << a[i] << " pop " << popular << endl;
                if (a[i] == popular || a[i+1] == popular)
                {
                    //cout << "cut " << a[i]+1 << " " << a[i+1]+1 << endl;
                    a[i] = -1;
                    a[i+1] = -1;
                    remaining--;
                }
            }

            //cout << "popular " << popular+1 << endl;
            //cout << "-----------" << endl;
            cutVertices++;
        }
        //cout << "cut " << cutVertices << endl;
    }
    return cutVertices;
}

int main(int argc, char* argv[])
{
    Graph graph;
    readFileIntoArray(&graph, argv[1]);
    //readFileIntoArray(&graph, "test4.txt");

    srand(time(NULL));
    int min = INT_MAX;
    for (int i = 0; i<25; i++)
    {
        //cout << "-----------" << endl;
        int temp = kargerMinCut(&graph);
        if (min>temp)
            min = temp;
    }
    cout << min << endl;

    return 0;
}
