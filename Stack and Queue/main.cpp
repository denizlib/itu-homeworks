#include "deck.h"
#include <time.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 20.12.2015
*/

using namespace std;

int main()
{
    cout << "The output has been written to file successfully!" << endl;
    freopen("output.txt", "w", stdout);
    srand(time(NULL));
    Stack tempDeck;
    int random;
    Deck deck;
    Queue specialDeck;
    tempDeck.create();
    deck.create();
    specialDeck.create();
    cout << "Shuffled deck:" << endl;
    deck.shuffle();
    Card holdCard;
    int stepNum=0;
    int i;
    cout << "Fortune-telling steps:" << endl;
    while (!deck.isempty())
    {
        for (i = 1; i <=13 ; i++)
        {
            if (!deck.isempty())
            {
                holdCard = deck.dequeue();
                if (i != holdCard.cardValue)
                {
                    tempDeck.push(holdCard);
                    // stepNum++;  to count how many steps have been taken
                    if (holdCard.cardValue == 1)
                        cout << i << ":" << holdCard.cardType << ":" << "A" << ":unmatch"<< endl;
                    else if (holdCard.cardValue == 11)
                        cout << i << ":" << holdCard.cardType << ":" << "J" << ":unmatch"<< endl;
                    else if (holdCard.cardValue == 12)
                        cout << i << ":" << holdCard.cardType << ":" << "Q" << ":unmatch"<< endl;
                    else if (holdCard.cardValue == 13)
                        cout << i << ":" << holdCard.cardType << ":" << "K" << ":unmatch"<< endl;
                    else
                        cout << i << ":" << holdCard.cardType << ":" << holdCard.cardValue << ":unmatch"<< endl;
                    if (i==13) // last possible case for tempDeck
                    {
                        while (!tempDeck.isempty())
                            tempDeck.pop();
                    }
                    continue;
                }
                else if (i == holdCard.cardValue)
                {
                    // stepNum++;  to count how many steps have been taken
                    if (holdCard.cardValue == 1)
                        cout << i << ":" << holdCard.cardType << ":" << "A" << ":match"<< endl;
                    else if (holdCard.cardValue == 11)
                        cout << i << ":" << holdCard.cardType << ":" << "J" << ":match"<< endl;
                    else if (holdCard.cardValue == 12)
                        cout << i << ":" << holdCard.cardType << ":" << "Q" << ":match"<< endl;
                    else if (holdCard.cardValue == 13)
                        cout << i << ":" << holdCard.cardType << ":" << "K" << ":match"<< endl;
                    else
                        cout << i << ":" << holdCard.cardType << ":" << holdCard.cardValue << ":match"<< endl;
                    specialDeck.enqueue(holdCard);
                    while (!tempDeck.isempty())
                    {
                        deck.enqueue(tempDeck.topStack());
                        tempDeck.pop();
                    }
                    break;
                }
            }
        }
    }
    cout << "Fortune Results: " << endl;
    int sum=0;
    Card temp;
    while (!specialDeck.isempty())
    {
        temp = specialDeck.dequeue();
        if (temp.cardValue == 1)
            cout << temp.cardType << ":" << "A" << endl;
        else if (temp.cardValue == 11)
            cout << temp.cardType << ":" << "J" << endl;
        else if (temp.cardValue == 12)
            cout << temp.cardType << ":" << "Q" << endl;
        else if (temp.cardValue == 13)
            cout << temp.cardType << ":" << "K" << endl;
        else
            cout << temp.cardType << ":" << temp.cardValue << endl;
        if (temp.cardValue == 1)
            temp.cardValue =10;
        if (temp.cardValue > 10)
            temp.cardValue =10;
        sum = sum + temp.cardValue;
    }
    cout << "Sum: " << sum << endl;
    if (0 <= sum < 50 )
        cout << "Bad luck, mate!" << endl;
    else if (sum != -1)
        cout << "Your wish will be granted!" << endl;
    fclose(stdout);
}


