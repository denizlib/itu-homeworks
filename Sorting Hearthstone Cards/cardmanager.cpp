#include "cardmanager.h"

void copyArray(CardManager &c_arr2, CardManager c_arr1)
{
    c_arr2.cardName = c_arr1.cardName;
    c_arr2.cardClass = c_arr1.cardClass;
    c_arr2.cardRarity = c_arr1.cardRarity;
    c_arr2.cardType = c_arr1.cardType;
    c_arr2.cardSet = c_arr1.cardSet;
    c_arr2.cardCost = c_arr1.cardCost;
}
bool compare(CardManager a, CardManager b)
{
    if (a.cardClass < b.cardClass)
        return false;
    else if (a.cardClass > b.cardClass)
        return true;
    else
    {
        if (a.cardCost < b.cardCost)
            return false;
        else if (a.cardCost > b.cardCost)
            return true;
        else
        {
            if (a.cardName < b.cardName)
                return false;
            else if (a.cardName > b.cardName)
                return true;
            else
                return true;
        }
    }
}

void CardManager::fullInsertionSort(CardManager* cardArray, int arraySize)
{
    int i, j;
    CardManager temp;
    CardManager* Cards = new CardManager[arraySize];
    Cards = cardArray;
    for (i = 1; i < arraySize; i++)
    {
       temp = cardArray[i];
       j = i-1;
       while (j >= 0 && compare(cardArray[j], temp))//cardArray[j].cardClass > temp.cardClass)
       {
           cardArray[j+1] =cardArray[j];
           j = j-1;
       }
       cardArray[j+1] = temp;
    }
}

void CardManager::filterInsertionSort(CardManager* cardArray, int arraySize)
{
    int i, j;
    CardManager temp;
    CardManager* Cards = new CardManager[arraySize];
    Cards = cardArray;
     for (i = 1; i < arraySize; i++)
    {
       temp =cardArray[i];
       j = i-1;
       while (j >= 0 && cardArray[j].cardType > temp.cardType )//cardArray[j].cardClass > temp.cardClass)
       {
           cardArray[j+1] = cardArray[j];
           j = j-1;
       }
       cardArray[j+1] =temp;
    }
    /*for (i = 1; i < arraySize; i++)
    {
       copyArray(temp, cardArray[i]);
       j = i-1;
       while (j >= 0 && cardArray[j].cardType > temp.cardType )//cardArray[j].cardClass > temp.cardClass)
       {
           copyArray(cardArray[j+1],cardArray[j]);
           j = j-1;
       }
       copyArray(cardArray[j+1], temp);
    }*/
}


void fullMerge(CardManager* Cards, int l, int m, int r)
{
    int i, j, k;
    int ln = m - l + 1;
    int rn =  r - m;

    CardManager* leftSet = new CardManager[ln];
    CardManager* rightSet = new CardManager[rn];

    for (i = 0; i < ln; i++)
        leftSet[i] = Cards[l + i];
    for (j = 0; j < rn; j++)
        rightSet[j] = Cards[m + 1+ j];

    i = 0;
    j = 0;
    k = l;
    while (i < ln && j < rn)
    {
        if (compare(rightSet[j],leftSet[i]))
        {
            Cards[k] = leftSet[i];
            i++;
        }
        else
        {
            Cards[k] = rightSet[j];
            j++;
        }
        k++;
    }

    while (i < ln)
    {
        Cards[k] = leftSet[i];
        i++;
        k++;
    }

    while (j < rn)
    {
        Cards[k] = rightSet[j];
        j++;
        k++;
    }
}

void filterMerge(CardManager* Cards, int l, int m, int r)
{
    int i, j, k;
    int ln = m - l + 1;
    int rn =  r - m;

    CardManager* leftSet = new CardManager[ln];
    CardManager* rightSet = new CardManager[rn];

    for (i = 0; i < ln; i++)
        leftSet[i] = Cards[l + i];
    for (j = 0; j < rn; j++)
        rightSet[j] = Cards[m + 1+ j];

    i = 0;
    j = 0;
    k = l;
    while (i < ln && j < rn)
    {
        if (rightSet[j].cardType >= leftSet[i].cardType)
        {
            Cards[k] = leftSet[i];
            i++;
        }
        else
        {
            Cards[k] = rightSet[j];
            j++;
        }
        k++;
    }

    while (i < ln)
    {
        Cards[k] = leftSet[i];
        i++;
        k++;
    }

    while (j < rn)
    {
        Cards[k] = rightSet[j];
        j++;
        k++;
    }
}

void CardManager::fullMergeSort(CardManager* Cards, int l, int r)
{
    if (l < r)
    {
        int m = l+(r-l)/2;
        fullMergeSort(Cards, l, m);
        fullMergeSort(Cards, m+1, r);
        fullMerge(Cards, l, m, r);
    }
}

void CardManager::filterMergeSort(CardManager* Cards, int l, int r)
{
    if (l < r)
    {
        int m = l+(r-l)/2;
        filterMergeSort(Cards, l, m);
        filterMergeSort(Cards, m+1, r);
        filterMerge(Cards, l, m, r);
    }
}
