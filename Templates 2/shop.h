/*
    040100137
    Batuhan Denizli
*/

#include <iostream>
#include <cstdlib>
#include <string>
#include "cookie.h"
#include "candy.h"
#include "icecream.h"


template <class T>
class Shop
{
private:
    T * shop;
    //calculate while adding
    int curSize;
    int initialSize;
    float totalCost;
    int discount;
    //void print();
public:
    void setDiscount(const int& _discount);
    void add(const T &objT);
    Shop (const T &objT);
    T& operator[](int x)
    {
            return shop[x];
    }

    friend ostream &operator<<(ostream &output, Shop &objShop)   // << Operator Overloading
    {
        cout << "**********************\nNumber of items:" << objShop.curSize << endl;
        for (int i=0; i< objShop.curSize; i++)
        {
            cout << i+1 << ": " << objShop.shop[i].getName() << " #" << objShop.shop[i].getAmount() << " Cost: " << objShop.shop[i].getCost() << endl << endl;
        }
        cout << "**********************\nTotal Cost: " << objShop.totalCost << endl;
        if (objShop.discount > 0)
        {
            cout << "Discount: " << objShop.discount << "%" << endl;
            cout << "Discount amount: -" << objShop.totalCost*(((float)objShop.discount)/100) << endl;
            cout << "Discounted cost: " << objShop.totalCost*(1 - ((float)objShop.discount/100)) << endl;
        }
        output << "**********************\n";
        return output;
    }


};


/*
template <class T>
Shop (const T &objT)
*/
