/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 20.12.2015
*/

#include "deck.h"
#include <stdlib.h>

using namespace std;

void Deck::create()
{
    front = 0; back = 0;
    counter = 0;
    for (int i = 0; i <52; i++)
    {
        cardArray[i].cardValue= i%13+1;
        if (i/13 < 1)
            cardArray[i].cardType='S';
        else if (i/13 < 2)
            cardArray[i].cardType='H';
        else if (i/13 < 3)
            cardArray[i].cardType='D';
        else if (i/13 < 4)
            cardArray[i].cardType='C';
    }
}

void Deck::shuffle()
{
    if (counter < 52)
    {
        random = rand()%52;
        if (cardArray[random].cardValue != -1)
        {
            counter++;
            cout << counter << ":" << cardArray[random].cardType << ":" << cardArray[random].cardValue << endl;
            enqueue(cardArray[random]);
            cardArray[random].cardValue = -1;

        }
        shuffle();
    }
}

