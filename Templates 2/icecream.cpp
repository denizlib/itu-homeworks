/*
    040100137
    Batuhan Denizli
*/

#include "icecream.h"

using namespace std;


//Setters

void Icecream::setAmount(const float& _amount)
{
    litre=_amount;
}
void Icecream::setPrice(const float& _price)
{
    pricePerLitre=_price;
}
void Icecream::setName(const string& _name)
{
    name=_name;
}

//Getters

const string Icecream::getName() const
{
    return name;
}
const float Icecream::getAmount() const
{
    return litre;
}
const float Icecream::getPrice() const
{
    return pricePerLitre;
}

//Constructor


Icecream::Icecream (string _name, float _amount, float _price)
{
    name=_name;
    litre=_amount;
    pricePerLitre=_price;
}

float Icecream::getCost()
{
    cost = litre*pricePerLitre;
    return cost;
}


