#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define NUM_THREADS	4

void *reception(int *_applicants)
{
    int i;
    for (int i=0; i<1000; i++)
    {
        if(_applicants[i]!=0)
        {
            printf("Applicant %d applied to the receptionist",i);
        }
    }
    pthread_exit((void*) t);
}

int main (int argc, char *argv[])
{
    int applicants[1000];
    int seconds;
    char line[100];
    FILE *iFile = fopen("input.txt","r");
    int numberApplicants=0;
    while(!feof(iFile))
    {
        if (fgets(line, 4, iFile) != NULL )
        {

            sscanf(line,"%d",&seconds);
            //printf("%d\n", seconds);
            applicants[numberApplicants] = seconds;
            numberApplicants++;
        }
    }
    fclose(iFile);
    pthread_t thread[NUM_THREADS];
    pthread_attr_t attr;
    int rc;
    long t;
    void *status;

    /* Initialize and set thread detached attribute */
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    rc = pthread_create(&thread[t], &attr, reception, applicants);
    if (rc)
    {
        printf("ERROR; return code from pthread_create() is %d\n", rc);
        exit(-1);
    }

    /* Free attribute and wait for the other threads */
    pthread_attr_destroy(&attr);
    for(t=0; t<NUM_THREADS; t++)
    {
        rc = pthread_join(thread[t], &status);
        if (rc)
        {
            printf("ERROR; return code from pthread_join() is %d\n", rc);
            exit(-1);
        }
        printf("Main: completed join with thread %ld having a status of %ld\n",t,(long)status);
    }

    printf("Main: program completed. Exiting.\n");
    pthread_exit(NULL);
}
