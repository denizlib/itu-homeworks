#include <sys/shm.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
//#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <signal.h>
#include <semaphore.h>
#include <fcntl.h>

#define KEYSHM1 1234
#define KEYSHM2 2345
#define KEYSHM3 3456
#define KEYSHM4 4567


int totalEmployees;
int sizeofMachine;


using namespace std;

int readFirstLine(string filename)
{
    int lineCounter = 0;
    ifstream input;
    input.open(filename.c_str());
    string line;
    string word;
    if (!input.is_open())
    {
        cout << "this file does not exist" <<endl;
        return -1;
    }
    while (getline(input, line))
    {
        istringstream tempLine(line);
        if (lineCounter == 0)
        {
            getline(tempLine, word, ' ');
            sizeofMachine = atoi(word.c_str());
            input.close();
            return sizeofMachine;
        }
        else
            break;
        lineCounter++;
    }
    input.close();
    return 0;
}

void readFileIntoArray(int * requests, string filename)
{
    int wordCounter = 0;
    int lineCounter = 0;
    ifstream input;
    input.open(filename.c_str());
    string line;
    string word;
    while (getline(input, line))
    {
        if (lineCounter != 0){
            istringstream tempLine(line);
            wordCounter = 0;
            while(tempLine >> word)
            {
                requests[wordCounter] = atoi(word.c_str());
                wordCounter++;
            }
        }
        
        lineCounter++;
    }
    totalEmployees = wordCounter;
    input.close();
}


int main (int argc, char *argv[])
{
    int* operationorder = new int;
    int process_num;
    sem_t *sem;
    pid_t f;
    int segment1, segment2, segment3, segment4;

    segment4 = shmget(KEYSHM4, sizeof(int), 0700|IPC_CREAT);

    operationorder = (int*)shmat(segment4,0,0);

    bool* status = new bool[2];
    status[0]=false;    //warning
    status[1]=false;    //finished
 
    bool served = false;

    int* requests = new int[10000];

    int* w = new int;

    

    *operationorder = 0;

    *w = readFirstLine(argv[1]);
    if (*w == -1) return 0;
    readFileIntoArray(requests, argv[1]);

    int requested_cup = 0;
    cout << "SIMULATION BEGINS" << endl;
    cout << "Current water level " << *w <<" cups" << endl;
    for (int i=0; i<totalEmployees; i++)
    {
        f = fork();  
        if (f == 0){
            requested_cup = requests[i];
            process_num = i;
            //cout << "employee " << getpid() <<" "<< process_num << endl;
            sleep(1);
            break;
        }
    }
    if (f != 0)
    {
        sem = sem_open("/coffeamachine", O_CREAT, 0700, 0);
        sem_post(sem);
        segment1 = shmget(KEYSHM1, sizeof(bool)*2, 0700|IPC_CREAT);// | IPC_EXCL | S_IRUSR | S_IWUSR);
        segment2 = shmget(KEYSHM2, sizeof(int)*10000, 0700|IPC_CREAT);// | IPC_EXCL | S_IRUSR | S_IWUSR);
        segment3 = shmget(KEYSHM3, sizeof(int), 0700|IPC_CREAT);// | IPC_EXCL | S_IRUSR | S_IWUSR);

        status = (bool*)shmat(segment1,0,0);
        requests = (int*)shmat(segment2,0,0);
        w = (int*)shmat(segment3,0,0);
        status[1] = false;
        *w = sizeofMachine;
        //cout << *w << endl;
        sem_wait(sem);
        while (true)
        {
            if (status[1] == true && *w > 1) break;
            if (status[0] == false) sleep(1);
            else if (status[0])
            {
                sem_post(sem);
                *w = sizeofMachine;
                status[0] = false;
                cout << "Employee " << getpid() << " wakes up and fills the coffea machine" << endl << flush;
                cout << "Current water level " << *w <<" cups" << endl << flush;
                sem_wait(sem);
            }
        }
        wait(NULL);
        shmdt(w);
        shmdt(operationorder);
        shmdt(status);
        shmdt(requests);
        cout << "SIMULATION ENDS" << endl;
    }
    if (f < 0) return 0;
    //printf("%d\n", f);
    if (f == 0)
    {
        segment1 = shmget(KEYSHM1, sizeof(bool)*2, 0);
        segment2 = shmget(KEYSHM2, sizeof(int)*10000, 0);
        segment3 = shmget(KEYSHM3, sizeof(int), 0);
        segment4 = shmget(KEYSHM4, sizeof(int), 0);

        served =false;

        status = (bool*)shmat(segment1,0,0);
        w = (int*)shmat(segment3,0,0);
        operationorder = (int*)shmat(segment4,0,0);

        sem = sem_open("/coffeamachine", 0);
        while (*operationorder <= process_num)
        {
            sleep(1);
            if (*operationorder == process_num)
            {
                sem_post(sem);
                cout << "Employee " << getpid() << " wants coffea Type " << requested_cup << endl << flush;
                cout.flush();
                while (served == false)
                {
                    
                    if (status[0] == true) continue;

                    if (*w - requested_cup < 1)
                    {
                        cout << "Employee " << getpid() << " WAITS" << endl << flush;
                        cout.flush();
                        status[0] = true;
                        sem_wait(sem);
                        sleep(1);
                    }
                    else
                    {
                        served = true;
                        cout << "Employee " << getpid() << " SERVED" << endl << flush;
                        cout.flush();
                        *w = *w - requested_cup;
                        cout << "Current water level " << *w <<" cups" << endl << flush;
                        cout.flush();
                        if (*w == 1) status[0] = true;
                        if (process_num == totalEmployees - 1) status[1] = true;
                        *operationorder = *operationorder + 1; 
                        sem_wait(sem);
                        shmdt(w);
                        shmdt(operationorder);
                        shmdt(status);
                    }
                }
            }
        }
    }
    return 0;
}
