/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 08.05.2016
*/
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>

using namespace std;

class Book
{
    public:
    string label;
    float weight, length, width;

    Book(string _label, float _weight, float _length, float _width)
    {
        label=_label;
        weight=_weight;
        length=_length;
        width=_width;
    }
    Book(){};
    friend std::ostream& operator<< (std::ostream& stream, const Book& _box)
    {
        stream << "Book Label: " << _box.label << " # " << _box.length << "x" << _box.width << " " << _box.weight;
        return stream;
    }
};
