/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 18.09.2014
*/
struct Faculty_Record {
    char fname[15], lname[15];
    int phone, office;
    char dept[5];
};
