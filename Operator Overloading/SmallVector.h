/*
    040100137
    Batuhan Denizli
*/
#ifndef SMALLVECTOR_H
#define SMALLVECTOR_H

#include<iostream>

class SmallVector
{
    public:
        SmallVector();
        SmallVector(int *incoming, int _size);
        virtual ~SmallVector();
        SmallVector(const SmallVector& other);
        SmallVector& operator=(const SmallVector& other);
        SmallVector& operator+(const SmallVector& inobject);
        SmallVector& operator*(int value);
        int& operator[](int index);
        const int& operator[](int index) const;
        SmallVector reverse();
        int pop_back();
        void push_back(int value);
        int getSize() const { return size; }
        void setSize(int val) { size = val; }
        int getCapacity() const { return capacity; }
        void setCapacity(int val) { capacity = val; }
        int getDynamicCapacity() const { return dynamicCapacity; }
    protected:
    private:
        int size;
        int capacity;
        int dynamicCapacity;
        int StaticBuffer[32];
        int *DynamicBuffer;
};

SmallVector& SmallVector::operator+(const SmallVector& inobject)
{
    SmallVector* newVector = new SmallVector(*this);
    for (int i = 0; i < inobject.getSize(); i++)
        {
            newVector->push_back(inobject[i]);
        }
    return *newVector;
}

/*SmallVector& SmallVector::operator*(int value)
{

}*/

int& SmallVector::operator[](int index)
{
    if (index >= 0)
    {
        if (size <= capacity)
        {
            return StaticBuffer[index];
        }
        else
        {
            return DynamicBuffer[index - capacity];
        }
    }
    else if (index < 0)
    {
        index *= -1;
        if (size <= capacity)
        {
            return StaticBuffer[size - index];
        }
        else
        {
            return DynamicBuffer[size - capacity - index];
        }
    }
}

const int& SmallVector::operator[](int index) const
{
    if (index >= 0)
    {
        if (size <= capacity)
        {
            return StaticBuffer[index];
        }
        else
        {
            return DynamicBuffer[index - capacity];
        }
    }
    else if (index < 0)
    {
        index *= -1;
        if (size <= capacity)
        {
            return StaticBuffer[size - index];
        }
        else
        {
            return DynamicBuffer[size - capacity - index];
        }
    }
}

SmallVector SmallVector::reverse()
{

}

int SmallVector::pop_back()
{
    if (size <= capacity)
    {
        return StaticBuffer[size-1];
        size--;
    }
    else
    {
        return DynamicBuffer[size-capacity-1];
        size--;
    }
}

void SmallVector::push_back(int value)
{
    if (size < capacity)
    {
        StaticBuffer[size] = value;
        size++;
    }
    else if (size == capacity)
    {
        DynamicBuffer = new int[dynamicCapacity];
        DynamicBuffer[0] = value;
        size++;
    }
    else if (size > capacity)
    {
        if ((size - capacity) == dynamicCapacity)
        {
            int* temp = new int[dynamicCapacity * 2];
            for (int i = 0; i < dynamicCapacity; i++)
            {
                temp[i] = DynamicBuffer[i];
            }
            delete DynamicBuffer;
            DynamicBuffer = temp;
        }
        else if ((size-capacity) < dynamicCapacity)
        {
            DynamicBuffer[size-capacity] = value;
        }
    }
}


SmallVector::SmallVector(int* incoming, int _size)
{
    size = _size;
    capacity = 32;
    dynamicCapacity = 32;  //default dynamic capacity
    while (dynamicCapacity < (size - capacity)) //in case dynamic capacity is not enough to hold extra elements
    {
        dynamicCapacity *= 2;
    }
    if (size <= capacity)
    {
        for (int i = 0; i < size; i++)
        {
            StaticBuffer[i] = incoming[i];
        }
    }
    else if (size > capacity)
    {
        DynamicBuffer = new int[dynamicCapacity];
        for (int i = 0; i < capacity; i++) // fill the static buffer
        {
            StaticBuffer[i] = incoming[i];
        }
        for (int i = 0; i < size - capacity; i++) //fill the dynamic buffer
        {
            DynamicBuffer[i] = incoming[i+capacity];
        }

    }
}


SmallVector::SmallVector()
{
    size = 0;
    capacity = 32;
    dynamicCapacity = 32;
}

SmallVector::~SmallVector()
{
    delete DynamicBuffer;
}

SmallVector::SmallVector(const SmallVector& incoming)
{
    size = incoming.getSize();
    capacity = incoming.getCapacity();
    dynamicCapacity = incoming.getDynamicCapacity();
    if (size <= capacity)
    {
        for (int i = 0; i < size; i++)
        {
            StaticBuffer[i] = incoming[i];
        }
    }
    else if (size > capacity)
    {
        DynamicBuffer = new int[dynamicCapacity];
        for (int i = 0; i < capacity; i++) // fill the static buffer
        {
            StaticBuffer[i] = incoming[i];
        }
        for (int i = 0; i < size - capacity; i++) //fill the dynamic buffer
        {
            DynamicBuffer[i] = incoming[i+capacity];
        }

    }
}

SmallVector& SmallVector::operator=(const SmallVector& incoming)
{
    if (this == &incoming) return *this; // handle self assignment
    size = incoming.getSize();
    capacity = incoming.getCapacity();
    dynamicCapacity = incoming.getDynamicCapacity();
    if (size <= capacity)
    {
        for (int i = 0; i < size; i++)
        {
            StaticBuffer[i] = incoming[i];
        }
    }
    else if (size > capacity)
    {
        DynamicBuffer = new int[dynamicCapacity];
        for (int i = 0; i < capacity; i++) // fill the static buffer
        {
            StaticBuffer[i] = incoming[i];
        }
        for (int i = 0; i < size - capacity; i++) //fill the dynamic buffer
        {
            DynamicBuffer[i] = incoming[i+capacity];
        }

    }
    return *this;
}


#endif // SMALLVECTOR_H

