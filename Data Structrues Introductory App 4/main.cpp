/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 13.10.2015
*/
#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <ctype.h>
#include "operations.h"
extern int sayac;
using namespace std;
typedef Library Datastructure;
Datastructure _library;
void main_menu();
bool do_operations(char);
void search_Author();
void search_Type();
void insert_book();
void remove_book();
void print_all();
void update_book();
int main()
{
    _library.create();
    bool bitir = false;
    char choice;
    while (!bitir)
    {
        main_menu();
        cin >> choice;
        bitir = do_operations(choice);
    }
    _library.close();
    return EXIT_SUCCESS;
}
void main_menu()
{
    cout << endl << endl;
    cout << "Please select the operation to perform and enter operation code" << endl;
    cout << "P: Print the whole catalogue" << endl;
    cout << "A: Search the catalogue by author" << endl;
    cout << "T: Search the catalogue by book type" << endl;
    cout << "I: Insert a new book record" << endl;
    cout << "U: Update an existing book record" << endl;
    cout << "R: Remove a book record" << endl;
    cout << "E: Exit the program" << endl;
    cout << endl;
    cout << "Your selection is: ";
}
bool do_operations(char secim)
{
    bool endProgram=false;
    switch (secim)
    {
        case 'P': case 'p':
            print_all();
            break;
        case 'A': case 'a':
            search_Author();
            break;
        case 'T': case 't':
            search_Type();
            break;
        case 'I': case 'i':
            insert_book();
            break;
        case 'U': case 'u':
            update_book();
            break;
        case 'R': case 'r':
            remove_book();
            break;
        case 'E': case 'e':
            cout << "Are you sure that you want to terminate the program? (Y/N):";
            cin >> secim;
            if(secim=='Y' || secim=='y')
                endProgram=true;
            break;
        default:
            cout << "Error: You have made an invalid choice" << endl;
            cout << "Try again {P, A, T, I, U, R, E} :" ;
            cin >> secim;
            endProgram = do_operations(secim);
            break;
    }
    return endProgram;
    }
void search_Author()
{
    char authorName[20];
    cout << "Please enter the name of the person you want to search (press '*' for listing all):" << endl;
    cin.ignore(1000, '\n');
    cin.getline(authorName,20);
    if(_library.searchAuthor(authorName)==0)
    {
        cout << "Record can not be found" << endl;
    }
    getchar();
};
void search_Type()
{
    char bookType[20];
    cout << "Please enter the type of book you want to search (press '*' for listing all):" << endl;
    cin.ignore(1000, '\n');
    cin.getline(bookType,20);
    if(_library.searchType(bookType)==0)
    {
        cout << "Record can not be found" << endl;
    }
    getchar();
};
void insert_book()
{
    book_record newRecord;
    cout << "Please enter the information of the book you want to save " << endl;
    cout << "Author Name : " ;
    cin.ignore(1000, '\n');
    cin.getline(newRecord.authorName,20);
    cout << "Author Surname : " ;
    cin.getline(newRecord.authorSurname,20);
    cout << "Book Title: ";
    cin.getline(newRecord.title,20);
    cout << "Book Type: ";
    cin.getline(newRecord.bookType,10);
    cout << "ISBN: ";
    cin >> newRecord.ISBN;
    cout << "Location: ";
    cin >> newRecord.location;
    _library.add(&newRecord);
    cout << "Record has been added" << endl;
    getchar();
    cin.clear();
    cin.ignore();
};
void remove_book()
{
    int ISBN;
    int choice;
    cout << "Please enter ISBN of the book you want to delete:" << endl;
    cin >> ISBN;
    int numberofFound=_library.searchISBN(ISBN);
    if(numberofFound==0)
    {
        cout << "Record can not be found" << endl;
    }
    else
    {
        if (numberofFound==1)
        {
            _library.removeBook(sayac);
            cout << "Record has been deleted" <<endl;
        }

    }
    getchar();
};
void update_book()
{
    int ISBN;
    cout << "Please enter ISBN of the book you want to update:" << endl;
    cin >> ISBN;
    int numberofFound=_library.searchISBN(ISBN);
    if(numberofFound==0)
    {
        cout << "Record can not be found" << endl;
    }
    else
    {
        if (numberofFound==1)
        {
            cout << "Record has been found." << endl;
            book_record newRecord;
            cout << "Please enter the up-to-date information" << endl;
            cout << "Author Name : " ;
            cin.ignore(1000, '\n');
            cin.getline(newRecord.authorName,20);
            cout << "Author Surname : " ;
            cin.getline(newRecord.authorSurname,20);
            cout << "Book Title: ";
            cin.getline(newRecord.title,20);
            cout << "Book Type: ";
            cin.getline(newRecord.bookType,10);
            cout << "Location: ";
            cin >> newRecord.location;
            newRecord.ISBN=ISBN;
            _library.update(sayac,&newRecord);
            cout << "Record has been updated successfully" <<endl;
        }
    }
    getchar();
};
void print_all()
{
    _library.printAll();
}
