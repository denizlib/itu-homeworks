/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 30.03.2016
*/
#define MAX_COURSES 10
#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <ctype.h>
using namespace std;
class Instructor
{
private:
    string title, firstName, lastName, email, username;
    int phoneNumber, roomNumber;
    string courses[MAX_COURSES];
public:
    Instructor(string _title="", string _firstName="", string _lastName="", int _phoneNumber=0, int _roomNumber=0, string _username="", string _email="", string* _courses[]={}, int _counter=0);
    string getTitle();
    string getFirstName();
    string getLastName();
    string getEmail();
    string getUsername();
    //string [] getCourses();
    int getPhoneNumber();
    int getRoomNumber();
    void setTitle(string);
    void setFirstName(string);
    void setLastName(string);
    void setEmail(string);
    void setUsername(string);
    void setPhoneNumber(int);
    void setRoomNumber(int);
};

Instructor::Instructor(string _title, string _firstName, string _lastName, int _phoneNumber, int _roomNumber, string _username, string _email, string* _courses[], int _counter)
{
    title=_title;
    firstName=_firstName;
    lastName=_lastName;
    phoneNumber=_phoneNumber;
    roomNumber=_roomNumber;
    username=_username;
    email=_email;
    /*for (int i=0; i < _counter ; i++)
    {*/
        courses=_courses;
    //}
}

string Instructor::getTitle()
{
    return title;
}
string Instructor::getFirstName()
{
    return firstName;
}
string Instructor::getLastName()
{
    return lastName;
}
string Instructor::getEmail()
{
    return email;
}
string Instructor::getUsername()
{
    return username;
}
int Instructor::getPhoneNumber()
{
    return phoneNumber;
}
int Instructor::getRoomNumber()
{
    return roomNumber;
}
void Instructor::setTitle(string _title)
{
    title=_title;
}
void Instructor::setFirstName(string _firstName)
{
    firstName=_firstName;
}
void Instructor::setLastName(string _lastName)
{
    lastName=_lastName;
}
void Instructor::setEmail(string _email)
{
    email=_email;
}
void Instructor::setUsername(string _username)
{
    username=_username;
}
void Instructor::setPhoneNumber(int _phoneNumber)
{
    phoneNumber=_phoneNumber;
}
void Instructor::setRoomNumber(int _roomNumber)
{
    roomNumber=_roomNumber;
}

