class Book{
    int isbn;
    string title,author;
public:
    Book(){}
    Book(const string&, const string&, int=0);
    int getISBN() const&;
    void setISBN(int);
    string getTitle() const;
    void setTitle(const string&);
    string getAuthor() const;
    void setAuthor(const string&);

    void print() const;
};


class Bookcase{
    Book *b_ptr;
    int curCap;
    const int maxCap;
    bool full;
public:
    Bookcase(int=50);
    Bookcase()
    void printAll() const;
    bool addBook(Book&);
    void findBook(int) const;
};
