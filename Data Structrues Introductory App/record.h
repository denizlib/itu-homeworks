#include <stdio.h>

struct PhoneNumber
{
	PhoneNumber* next;
	int number;
	char* name;
	PhoneNumber(char* _name, int _number, PhoneNumber* _next)
	{
		name = _name;
		number = _number;
		next = _next;
	}
};
struct Person
 {
	char* name;
	Person* next;
	Person* prev;
	PhoneNumber* phoneNumbers;
	Person(char* _name, Person* _next, Person* _prev)
	{
		name = _name;
		next = _next;
		prev = _prev;
		phoneNumbers  = null;
	}
};
struct PhoneBook
 {
	Person* head;
	void addPerson();
	void addNumber();
void removePerson();
void removeNumber();
void updatePerson();
void updateNumber();
int search();
void list();
};

