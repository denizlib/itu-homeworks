#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "tree.h"
#define ARRAY_SIZE 10
#include <time.h>
#include <math.h>

using namespace std;

void printPreorder(node *tptr);
tree* createTree();
void insertNode(node* parent, int newValue, int height, int depth, int size);
node* createNode(int value);

int main()
{
    srand(time(NULL));
    printPreorder(createTree()->root);
    if(0)
        cout << "lol " << endl;
    return 0;
}


tree* createTree()
{
    int random;
    int randomArray[ARRAY_SIZE];
    for (int i=0; i<ARRAY_SIZE ; i++)
    {
        random=rand()%ARRAY_SIZE;
        randomArray[i]=random+1;
    }
    tree* treemendus = new tree;
    node *p = new node;
    p->right = NULL;
    p->left = NULL;
    treemendus->root = p;
    int size=0;
    for (int i=0; i<ARRAY_SIZE ; i++)
    {
        int depth = log2(i+1);
        insertNode(p, randomArray[i], depth, depth, i);
    }
    return treemendus;
}

void insertNode(node* parent, int newValue, int height, int depth, int size)
{
    int diff = depth-height;
    if (size==0)
        parent->number = newValue;
    else
        if(((size+1)%((int)(pow(2,diff+1))))/((int)pow(2, diff)))
            if (parent->right)
                insertNode(parent->right, newValue, height-1, depth, size);
            else
                parent->right = createNode(newValue);
        else
            if (parent->left)
                insertNode(parent->left, newValue, height-1, depth, size);
            else
                parent->left = createNode(newValue);
}

void printPreorder(node *tptr)
{
	if (tptr) {
		cout << tptr->number << endl;
		printPreorder(tptr->left);
		printPreorder(tptr->right);
	}
}

node* createNode(int value)
{
    node* newnode = new node;
    newnode->right = NULL;
    newnode->left = NULL;
    newnode->number = value;
    return newnode;
}
