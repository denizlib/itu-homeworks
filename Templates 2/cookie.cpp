/*
    040100137
    Batuhan Denizli
*/

#include "cookie.h"

using namespace std;


//Setters

void Cookie::setAmount(const float& _amount)
{
    amount=_amount;
}
void Cookie::setPrice(const float& _price)
{
    pricePerDozen=_price;
}
void Cookie::setName(const string& _name)
{
    name=_name;
}

//Getters

const string Cookie::getName() const
{
    return name;
}
const float Cookie::getAmount() const
{
    return amount;
}
const float Cookie::getPrice() const
{
    return pricePerDozen;
}

//Constructor


Cookie::Cookie (string _name, float _amount, float _price)
{
    name=_name;
    amount=_amount;
    pricePerDozen=_price;
}

float Cookie::getCost()
{
    cost = amount*(pricePerDozen/12);
    return cost;
}


/*
float Cookie::totalCost()
{

    return amount*(pricePerDozen/12);
}
*/
