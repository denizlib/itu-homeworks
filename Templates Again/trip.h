/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 04.05.2015
*/
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <vector>
#include <exception>
using namespace std;

template <class Type>

class Trip
{
	private :
		double discountAmount;
		double discount=0;
		double cost;
		double totalCost=0;
		int size;
        Type* typeArray;
		int length;
	public:
		Trip(Type& t)
		{
			size = 2;
			//totalCost=0;
			typeArray = new Type[size];
			length = 0;
			typeArray[length] = t;
			length++;
		}
		/*template <class Type2> class Trip2    // MultipleTrip does not work.
		{
            private :
                double totalCost;
            public :
                Trip2(const Type& t)
                {

                }
		};*/
		void operator+=(Trip& T)
		{
			int newSize = size;
			while ((T.num()+length) > newSize)
				newSize *= 2;
			if(newSize > size)
			{
				Type* tempArray = new Type[newSize];
				for(int i=0; i<length; i++)
					tempArray[i] = typeArray[i];
				size = newSize;
				delete[] typeArray;
				typeArray = tempArray;
			}
			for(int i=0; i<T.num(); i++)
				typeArray[i+length] = T[i];
			length += T.num();
		}

		Trip(Type *_typeArray, int num)
		{
		    //totalCost=0;
		    length=0;
		    int newSize = 2;
			while (num+length > newSize)
				newSize *= 2;
			typeArray = new Type[newSize];
			for(int i=0; i<num; i++)
				typeArray[i] = _typeArray[i];
			size = newSize;
			length = num;
			cost=0;
			discountAmount=0;
		}
		~Trip()
		{
			delete[] typeArray;
		}
		Type & operator[](int index)
		{
			if(index >= length)
				throw string("Index out of bound!");
			return typeArray[index];
		}
		void add(Type& t)
		{
			if(length == size)
			{
				Type* tempArray = new Type[2*size];
				for(int i=0; i<size; i++)
					tempArray[i] = typeArray[i];
				size *= 2;
				delete[] typeArray;
				typeArray = tempArray;
			}
			typeArray[length] = t;
			length++;
		}
		int num()
		{
			return length;
		}
		/*const Type& get() const
		{
			return typeArray;
		}*/
		void setDiscount(double _discount)
		{
			if (_discount < 0 || _discount >= 40)
			{
				cout << "Discount out of range!" << endl;
				return;
			}
			else
                discount = _discount;
			/*else if (_discount > 0 && _discount <=40)
			{

				cout << "Discount: %" << discount << endl;
				cout << "Discount amount: " << discountAmount << endl;
				cout << "Discounted cost: " << cost << endl;
			}*/
		}
		friend ostream &operator<<( ostream &output, Trip &T )
		{
			//cout << "11111" << endl;
			//cout >> D.get().size();
			cout << "*****************************************" << endl << endl;
			cout << "Trip transfer count: " << T.num() << endl;
			for(int i=0; i<T.num(); i++)
            {
                T.totalCost = T.totalCost + T[i].cost;
                output << i+1 << ":" << T[i] << endl;
            }
            cout << "*****************************************" << endl << endl;
            cout << "Trip Total Cost:" << T.totalCost << endl;
 			if (T.discount > 0 && T.discount <=40)
            {
                T.discountAmount = T.totalCost * T.discount * 0.01;
				T.totalCost = T.totalCost - T.discountAmount;
                cout << "Discount: %" << T.discount << endl;
				cout << "Discount amount: -" << T.discountAmount << endl;
				cout << "Discounted cost: " << T.totalCost << endl;
            }
            cout << "*****************************************" << endl << endl;
            T.totalCost=0;
			return output;
		}
		/*void toString()
		{
			string str;
			for(int i=0; typeArray->size(); i++)
			{
				ostringstream strs;
				strs << i;
				str.append(strs.str());
				str.append(". ");
				str.append(typeArray[i].toString());
				str.append("\n");
			}


		}*/

};
