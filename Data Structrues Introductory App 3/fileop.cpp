/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 18.09.2014
*/
#include "fileop.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;

void Dosya::ekle(Faculty_Record *ananzaa){
    fseek(facultyrecord, 0, SEEK_END);
    fwrite(ananzaa, sizeof(Faculty_Record), 1, facultyrecord);
}

void Dosya::olustur(){
    dosyaadi="fakultekayitlari.txt";
    facultyrecord = fopen(dosyaadi, "r+" );
    if(!facultyrecord){
        if(!(facultyrecord = fopen( dosyaadi, "w+" ))){
            cerr << "File can not be opened" << endl;
            exit(1);
        }
    }
}

void Dosya::kapat(){
    fclose(facultyrecord);
}

void Dosya::guncelle(int kayitno, Faculty_Record *ananzaa){
    if(fseek(facultyrecord, sizeof(Faculty_Record)*(kayitno-1), SEEK_SET)==0)
        fwrite(ananzaa, sizeof(Faculty_Record), 1, facultyrecord);
}

void Dosya::sil(int kayitno){
    fseek(facultyrecord, 0, SEEK_END);
    int size=ftell(facultyrecord)-(kayitno*sizeof(Faculty_Record));
    fseek(facultyrecord, sizeof(Faculty_Record)*(kayitno), SEEK_SET);
    char buffer[size];
    Faculty_Record boskayit={"",""};
    fread(buffer, sizeof (Faculty_Record), 1, facultyrecord);
    fseek(facultyrecord, sizeof(Faculty_Record)*(kayitno-1), SEEK_SET);
    fwrite(buffer, size, 1, facultyrecord);
    cout<< "SIZE" <<size << "sof" << sizeof (Faculty_Record);
    int pos=ftell(facultyrecord)-(sizeof(Faculty_Record));
    fseek(facultyrecord, pos, SEEK_SET);
    fwrite(&boskayit, sizeof(Faculty_Record), 1, facultyrecord);



  /*  Faculty_Record boskayit={"",""};


    if(fseek(facultyrecord, sizeof(Faculty_Record)*(kayitno-1), SEEK_SET)==0)
        fwrite(&boskayit, sizeof(Faculty_Record), 1, facultyrecord);

    while(!feof(facultyrecord)){
        sayac++;
        fread( &adk, sizeof (Faculty_Record), 1, facultyrecord);
        if(feof(facultyrecord)) break;

        if(!tumu && strnicmp(adk.fname, aranan, strlen(aranan))!=0)
            continue;
    }*/
}

/*int Dosya::maintenance(){
    char *tempname = "tempfacultyrecords.txt";
    FILE *tempfile;
    Faculty_Record k;
    int counter=0;
    tempfile = fopen(tempname, "w+");
    if (!tempfile){
        cerr << "Temporary file cannot be opened." << endl;
        exit(1);
    }
    fseek(facultyrecord, 0, SEEK_SET);
    while(!feof(facultyrecord)){
        fread( &k, sizeof (Faculty_Record), 1, facultyrecord);
        if(feof(facultyrecord)) break;
        if((strcmp(k.fname,"")==0))
            counter++;
        else
            fwrite(&k, sizeof(Faculty_Record), 1, facultyrecord);
    }
    if (counter>0){
        fclose(facultyrecord);
        fclose(tempfile);
        char command[500]="copy ";
        strcat(command,tempname);
        strcat(command," ");
        strcat(command,dosyaadi);
        strcat(command);
        create();
    }
}*/

int Dosya::araAd(char aranan[]){
    Faculty_Record adk;
    int sayac=0;
    bool tumu=false;
    int bulunan=0;
    if(strcmp(aranan,"*")==0)
        tumu=true;
    fseek(facultyrecord, 0, SEEK_SET);
    while(!feof(facultyrecord)){
        sayac++;
        fread( &adk, sizeof (Faculty_Record), 1, facultyrecord);
        if(feof(facultyrecord)) break;

        if(!tumu && strnicmp(adk.fname, aranan, strlen(aranan))!=0)
            continue;
        cout << sayac << "." << adk.fname << " " << adk.lname << " " << adk.phone << " " << adk.office << " " << adk.dept << endl;
        bulunan++;
    }
    return bulunan;
}

int Dosya::araDept(char aranan2[]){
    Faculty_Record deptk;
    int sayac=0;
    bool tumu=false;
    int bulunan=0;
    if(strcmp(aranan2,"*")==0)
        tumu=true;
    fseek(facultyrecord, 0, SEEK_SET);
    while(!feof(facultyrecord)){
        sayac++;
        fread( &deptk, sizeof (Faculty_Record), 1, facultyrecord);
        if(feof(facultyrecord)) break;

        if(!tumu && strnicmp(deptk.dept, aranan2, strlen(aranan2))!=0)
            continue;
        cout << sayac << "." << deptk.dept << " " << deptk.fname << " " << deptk.lname << " " << deptk.phone << " " << deptk.office << endl;
        bulunan++;
    }
    return bulunan;
}

Faculty_Record Dosya::getir(int kayitno){
    Faculty_Record deptk;
    fseek(facultyrecord, sizeof(Faculty_Record)*(kayitno-1), SEEK_SET);
    fread(&deptk, sizeof(Faculty_Record), 1, facultyrecord);
    return deptk;
}
