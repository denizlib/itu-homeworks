/*
    040100137
    Batuhan Denizli
*/

#ifndef ICECREAM
#define ICECREAM

#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

class Icecream
{
private:
    float litre;
    float pricePerLitre;
    string name;
    float cost;
public:
    Icecream (){};
    Icecream (string _name, float _amount, float _price);
    void setAmount(const float& _amount);
    void setPrice(const float& _price);
    void setName(const string& _name);
    string const getName() const;
    float const getAmount() const;
    float const getPrice() const;
    float getCost();
    friend ostream& operator<<(ostream &output,Icecream &obj) // cout << cookieShop[2] << endl;
    {
        if(obj.cost>0) //to check if it's empty
        {
            output << "Requested Icecream: " << obj.getName() << " #" << obj.getAmount() << " Cost: " << obj.getCost() <<  "  (Before taxes and discount)" << endl;
        }
        else
        {
            output << "We don't have enough icecream!";
        }
        return output;
    }
};

#endif // ICECREAM
