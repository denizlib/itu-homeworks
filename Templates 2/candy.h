/*
    040100137
    Batuhan Denizli
*/

#ifndef CANDY
#define CANDY


#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

class Candy
{
private:
    float weight;
    float pricePerKG;
    string name;
    float cost;
public:
    Candy (){};
    Candy (string _name, float _amount, float _price);
    void setAmount(const float& _amount);
    void setPrice(const float& _price);
    void setName(const string& _name);
    string const getName() const;
    float const getAmount() const;
    float const getPrice() const;
    float getCost();
    friend ostream& operator<<(ostream &output,Candy &obj) // cout << cookieShop[2] << endl;
    {
        if(obj.cost>0) //to check if it's empty
        {
            output << "Requested Candy: " << obj.getName() << " #" << obj.getAmount() << " Cost: " << obj.getCost() << "  (Before taxes and discount)" << endl;
        }
        else
        {
            output << "We don't have enough candies!";
        }
        return output;
    }
};


#endif
