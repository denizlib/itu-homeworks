/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 04.05.2015
*/
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <sstream>
using namespace std;
#ifndef FLIGHT_H_
#define FLIGHT_H_

class Flight
{
private:
	string journey;
	string flightNumber;
public:
    double cost;
	Flight()
	{

	};
	Flight(const char* _journey, const char* _flightNumber, double _cost)
	{
		journey = string(_journey);
		flightNumber= string(_flightNumber);
		cost = _cost;
	}
	void setCost(double _cost)
	{
		cost = _cost;
	}
	/*
	string toString()
	{
		string str="Flight Name: ";
		str.append(journey);
		str.append(" #");
		str.append(flightNumber);
		str.append(" Cost: ");
		ostringstream strs;
		strs << cost;
		str.append(strs.str());
		return str;
	}
	*/
	friend ostream &operator<<( ostream &output, const Flight &F)
	{
		output << "Flight Name: " << F.journey << " #" << F.flightNumber << " Cost: " << F.cost;
		return output;
	}

};


#endif /* FLIGHT_H_ */
