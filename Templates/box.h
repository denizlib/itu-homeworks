/*
* @Author
* Student Name: Batuhan Denizli
* Student ID : 040100137
* Date: 08.05.2016
*/
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <vector>

using namespace std;

template <class T> class Box
{

    public:
    //T b[100];
    float weight, length, width, allowed, currentWeight;
    vector<T> boxV;

    Box (float _weight, float _length, float _width , float _allowed)
    {
        weight=_weight;
        length=_length;
        width=_width;
        allowed=_allowed;
    }
    Box()
    {
    }
    T add(T& obj);
    T add(T* obj, int j);
    T operator[](int i);
    //T operator<<(T _box);
    friend std::ostream& operator<< (std::ostream& stream, const Box<T> _box)
    {
        for (float i=0; i < _box.boxV.size(); i++)
        {
            stream << _box.boxV[i] << endl;
        }
        return stream;
    }
};

template <class T> T Box<T>::add(T& obj)
{
    if ( obj.width < width && obj.length < length && obj.weight+currentWeight < allowed )
    {
        boxV.push_back(obj);
    }
    else if ( obj.width < length && obj.length < width && obj.weight+currentWeight < allowed )
    {
        boxV.push_back(obj);
    }
}

template <class T> T Box<T>::add(T* obj, int j)
{
    for (int i = 0; i<j ; i++)
    {
        if ( obj[i].width < width && obj[i].length < length && obj[i].weight+currentWeight < allowed)
        {
            boxV.push_back(obj[i]);
        }
        else if ( obj[i].width < length && obj[i].length < width && obj[i].weight+currentWeight < allowed )
        {
            boxV.push_back(obj[i]);
        }
    }
}


template <class T> T Box<T>::operator[](int i)
{
    return boxV[i];
}

/*
template <class T> T Box<T>::operator<<(T _box)
{
    if (_box.hasBattery==true)
        cout << 'Toy Label: ' << _box.label << ' # ' << _box.length << 'x' << _box.width << ' ' << _box.weight << 'kg Contains Battery' << endl;
    else
        cout << 'Toy Label: ' << _box.label << ' # ' << _box.length << 'x' << _box.width << ' ' << _box.weight << 'kg No Battery' << endl;
}

*/
