/*
ID: 040100137
Name: Batuhan Denizli
*/

#include <iostream>
#include <string>
#include <string.h>
#include <stdio.h>
#include "classes.h"

using namespace std;

Seat seat;

class SystemManager
{
    public:
        void createAirport(string);
        void createAirline(string);
        void createFlight(string, string, string, int, int, int, string);
        void createSection(string, string, int, int, int);
        void findAvailableFlights(string, string);
        void bookSeat(string, string, Seat, int, char);
        void displaySystemDetails();
};

void SystemManager::createAirport(string n)
{
    if (n.size() != 3)
    {
        cout << "Invalid Airport Name";
        return;
    }
    Airport *airport = new Airport;
    airport->name == n ;
}

void SystemManager::createAirline(string n)
{
    Airline *airline = new Airline;
    airline->name == n;
}

void SystemManager::createFlight(string aname, string orig, string dest, int year, int month, int day, string id)
{
    Flight *flight = new Flight;
    flight->name == aname;
    flight->orig == orig;
    flight->dest == dest;
    flight->year == year;
    flight->month == month;
    flight->day == day;
    flight->id == id;
}

void createSection(string air, string flID, int row, int col, int s)
{
    FlightSection *section = new FlightSection;
    section->seatClass = s;
    section->rows = row;
    section->cols = col;
}

bool FlightSection::hasAvailableSeats()
{
    //Seat seat;// = new Seat;
    for (int i = 1; i <=100; i++)
    {
        for (int j=1; j<=10; j++)
        {
            if (seat.seatStatus[i][j] == true)
            {
                return true;
            }
        }
    }
    return false;
}

void bookSeat(string, string, Seat, int, char)
{

}


void SystemManager::displaySystemDetails()
{

}




