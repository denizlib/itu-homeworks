
#include "artraverser.h"
#include <string.h>
#include <iostream>

using namespace std;

class AdderRemover
{
    public:
    Node* head=NULL;
    Node* tail=NULL;
    string name;
    int nodeCount;
    ARTraverser* traverser;
    virtual void add(int) = 0;
    virtual void remove() = 0;
    virtual void setTraverser() = 0;
    void display();
    void removeAll();
    void traverse();
};



class FAFR : public AdderRemover
{
    public:
    void add(int value)
    {
        if (head == NULL)
        {
            nodeCount=0;
            name = "FAFR";
            head = new Node();
            tail = new Node();
            head->data=value;
            tail->data=value;
            head = tail;
            nodeCount++;
        }
        else if (head != NULL)
        {
            Node* temp = new Node();
            head->prev = temp;
            temp->next = head;
            head = temp;
            head->data = value;
            nodeCount++;
        }
    }
    void remove()
    {
        if(nodeCount>0)
        {
            Node* temp = new Node();
            temp = head;
            if (head->next != NULL)
            {
                head = head->next;
                nodeCount--;
            }
            else if(head->next == NULL)
            {
                head = NULL;
                tail = NULL;
                nodeCount = 0;
            }
            delete[] temp;
        }
        else
            cout << "There is nothing to remove" << endl;
    }
    void setTraverser()
    {
        traverser = new ARTF();
        traverser->current = head;
    }
};

class FALR : public AdderRemover
{
    public:
    void add(int value)
    {
        if (head == NULL)
        {
            nodeCount=0;
            name = "FALR";
            head = new Node();
            tail = new Node();
            head->data=value;
            tail->data=value;
            head = tail;
            nodeCount++;
        }
        else if (head != NULL)
        {
            Node* temp = new Node();
            head->prev = temp;
            temp->next = head;
            head = temp;
            head->data = value;
            nodeCount++;
        }
    }
    void remove()
    {
        if(nodeCount>0)
        {
            Node* temp = new Node();
            temp = tail;
            if (tail->prev != NULL)
            {
                tail = tail->prev;
                nodeCount--;
            }
            else if(tail->prev == NULL)
            {
                head = NULL;
                tail = NULL;
                nodeCount=0;
            }
            delete[] temp;
        }
        else
            cout << "There is nothing to remove" << endl;
    }
    void setTraverser()
    {
        traverser = new ARTF();
        traverser->current = head;
    }
};

class LAFR : public AdderRemover
{
public:

    void add(int value)
    {
        if (head == NULL)
        {
            nodeCount=0;
            name = "LAFR";
            head = new Node();
            tail = new Node();
            head->data=value;
            tail->data=value;
            head = tail;
            nodeCount++;
        }
        else if (head != NULL)
        {
            Node* temp = new Node();
            tail->next = temp;
            temp->prev = tail;
            tail = temp;
            tail->data = value;
            nodeCount++;
        }
    }
    void remove()
    {
        if(nodeCount>0)
        {
            Node* temp = new Node();
            temp = head;
            if (head->next != NULL)
            {
                head = head->next;
                nodeCount--;
            }
            else if(head->next == NULL)
            {
                head = NULL;
                tail = NULL;
                nodeCount = 0;
            }
            delete[] temp;
        }
        else
            cout << "There is nothing to remove" << endl;
    }
    void setTraverser()
    {
        traverser = new ARTL();
        traverser->current = tail;
    }
};

class LALR : public AdderRemover
{
    public:
    void add(int value)
    {
        if (head == NULL)
        {
            nodeCount=0;
            name = "LALR";
            head = new Node();
            tail = new Node();
            head->data=value;
            tail->data=value;
            head = tail;
            nodeCount++;
        }
        else if (tail != NULL)
        {
            Node* temp = new Node();
            tail->next = temp;
            temp->prev = tail;
            tail = temp;
            tail->data = value;
            nodeCount++;
        }
    }
    void remove()
    {
        if(nodeCount>0)
        {
            Node* temp = new Node();
            temp = tail;
            if (tail->prev != NULL)
            {
                tail = tail->prev;
                nodeCount--;
            }
            else if(tail->prev == NULL)
            {
                head = NULL;
                tail = NULL;
                nodeCount=0;
            }
            delete[] temp;
        }
        else
            cout << "There is nothing to remove" << endl;
    }
    void setTraverser()
    {
        traverser = new ARTL();
        traverser->current = tail;
    }
};

void AdderRemover::traverse()
{
    cout << endl;
    cout << name << " | NodeCount:" << nodeCount << endl;
    cout << "------" << endl;
    if (nodeCount == 0)
        cout << "There is no element to print" << endl;
    else
    {
        for(int i = 0; i < nodeCount ; i++)
        {
            cout << traverser->next() << endl;
        }
    }

}

void AdderRemover::display()
{
    cout << endl;
    cout << this->name << " | NodeCount:" << nodeCount << endl;
    cout << "------" << endl;
    if (nodeCount == 0)
        cout << "There is no element to print" << endl;
    else
    {
        Node* temp = new Node;
        temp = head;
        for(int i = 0; i < nodeCount ; i++)
        {
            cout << temp->data << endl;
            temp = temp->next;
        }
    }
}

void AdderRemover::removeAll()
{
    Node* temp = new Node;
    if (nodeCount == 0)
    {
        delete[] temp;
        cout << "There is no element to remove" << endl;
    }

    for(int i = 0; i < nodeCount ; i++)
    {
        temp = head;
        head = head->next;
        delete[] temp;
    }
    head = NULL;
    tail = NULL;
    nodeCount = 0;
}


