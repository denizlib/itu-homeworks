/*
ID: 040100137
Name: Batuhan Denizli
*/

#include <iostream>
#include <string>
#include <string.h>
#include <stdio.h>
#include "SystemManager.h"

using namespace std;

int main() {
    for (int i = 1; i <=100; i++)
    {
        for (int j=1; j<=10; j++)
        {
            seat.seatStatus[i][j] = true;
        }
    }
    SystemManager res;
    res.createAirport("DEN");
    res.createAirport("dfw");
    res.createAirport("LON");
    res.createAirport("DEN"); //invalid, same name created before
    res.createAirport("denw"); //invalid more than 3 chars
    res.createAirline("delta");
    res.createAirline("AMER");
    res.createAirline("FRONT");
    res.createAirline("front"); //invalid same name created before
    res.createFlight("DELTA", "DEN", "LON", 2013, 10, 10, "123");
    res.createFlight("DELTA", "DEN", "DEH", 2013, 8, 8, "567abc");
    res.createFlight("DEL", "DEN", "LON", 2013, 9, 8, "567");  //invalid airline
    res.createFlight("DELTA", "LON33", "DEN33", 2013, 5, 7, "123"); //invalid airports
    res.createFlight("AMER", "DEN", "LON", 2010, 40, 100, "123abc"); //invalid date
    res.createSection("DELTA","123", 2, 2, 3);
    res.createSection("DELTA","123", 2, 3, 1);
    res.createSection("DELTA","123", 2, 3, 1);//Invalid
    res.createSection("SWSERTT","123", 5, 5, 3);//Invalid airline
    res.bookSeat("DELTA", "123", 1, 1, 'A');
    res.bookSeat("DELTA", "123", 3, 1, 'A');
    res.bookSeat("DELTA", "123", 3, 1, 'B');
    res.bookSeat("DELTA888", "123", 2, 1, 'A'); //Invalid airline
    res.bookSeat("DELTA", "123haha7", 2, 1, 'A'); //Invalid flightId
    res.bookSeat("DELTA", "123", 3, 1, 'A'); //already booked
    res.displaySystemDetails();
    res.findAvailableFlights("DEN", "LON");*/
    return 0;
}
